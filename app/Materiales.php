<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materiales extends Model
{
     protected $table = 'materiales';
     const CREATED_AT = 'fecha_creacion';
     const UPDATED_AT = 'fecha_actualizacion';


     public function material_tomado()
     {
          return $this->belongsTo('App\Material_tomado', 'id', 'material_id');
     }
}
