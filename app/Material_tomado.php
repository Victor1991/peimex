<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_tomado extends Model
{
     protected $table = 'material_tomado';
     const CREATED_AT = 'fecha_creacion';
     const UPDATED_AT = 'fecha_actualizacion';
}
