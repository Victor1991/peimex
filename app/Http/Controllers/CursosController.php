<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cursos;
use App\Clientes;
use App\Secciones;
use App\Materiales;
use App\Preguntas;
use App\Respuestas;
use App\Respuestas_alumnos;
use App\Material_tomado;
use App\Alumnos_cursos;
use App\Specialkey;
use App\Intentos;
use App\Material_introductorio;
use App\Medicos_participantes_cursos;
use App\Recurso_relacionados_curso;
use Carbon\Carbon;

use App\Examen;


use Illuminate\Support\Facades\DB;




class CursosController extends Controller
{

     public function index($type = 'mosaico'){
          $id = \Auth::user()->id;
          $cursos = Alumnos_cursos::where('usuario_id', $id)->get()->toArray();
          $data['cursos'] = array();
          if (count($cursos) > 0) {
               foreach ($cursos as $key => $curso) {
                    $curso = Cursos::find($curso['curso_id']);
                    if ($curso) {
                         $curso = $curso->toArray();
                         $data['cursos'][$key] = $curso;
                         $data['cursos'][$key]['porcentaje'] = $this->porcentaje_curso($curso['id']);
                         $data['cursos'][$key]['cliente'] = Clientes::find($curso['cliente_id'])->toArray();
                         $data['cursos'][$key]['ultimo_material'] = $this->ultimo_material($curso['id']);
                         $data['cursos'][$key]['aprobado'] = $this->examen_finalizado($curso['id']);
                         $data['cursos'][$key]['num_modulos'] = Secciones::where('curso_id', $curso['id'])->count();
                         $data['cursos'][$key]['examen'] = Examen::where('curso_id', '=', $curso['id'])->where('tipo', '=',  '1')->first();
                         if ($data['cursos'][$key]['examen']) {
                              $data['cursos'][$key]['examen']['n_preg'] = Preguntas::where('examen_id', '=',$data['cursos'][$key]['examen']['id'])->count();
                         }
                         // dump($data['cursos'][$key]['examen']);
                         $data['cursos'][$key]['ultimo_intento'] = $this->ultimo_intenoto_curo($curso['id']);
                    }
               }
          }

          $columns = array_column($data['cursos'], 'ultimo_material');
          $dt = array_multisort($columns, SORT_DESC, $data['cursos']);
          $data['type'] = $type;
          return view('home', $data);
     }




     public function porcentaje_curso($curso_id){
          $ultimo_intento = $this->ultimo_intenoto_curo($curso_id);
          $material = Materiales::select('id')->where('curso_id', $curso_id)->get()->toArray();
          $ids = array_column($material, 'id');
          $mat_fin = Material_tomado::select('material_id')->whereIn('material_id', $ids)->where('usuario_id', \Auth::user()->id)->where('finalizado', 1)->where('intento_id', $ultimo_intento['id'])->get()->toArray();
          $ultimo_material =  $this->ultimo_material($curso_id);;
          $finalizados = count($mat_fin);
          $examen = $this->examen_finalizado($curso_id);
          $total =  count($material);
          if ($finalizados > 0) {
               $porcentaje  = ($finalizados ) * 100 / (count($material));
          }else{
               $porcentaje = 0;
          }

          return array( round($porcentaje, 2), $total , $finalizados, $mat_fin, $ultimo_material);
     }

     public function examen_finalizado($curso_id)
     {
          $ultimo_intento = $this->ultimo_intenoto_curo($curso_id);

          $usuario_id = \Auth::user()->id;
          $aprobado =  DB::table('examen')->leftJoin('intentos', 'examen.id', '=', 'intentos.examen_id')->where('examen.curso_id', $curso_id)->where('intentos.id', $ultimo_intento['id'])->where('intentos.usuario_id', $usuario_id)->whereIn('examen.tipo', ['1', '3'])->first();
          if (is_null($aprobado)) {
               return 0;
          }else{
               return 1;
          }
     }

     public function curso($id){
          $curso = Cursos::find($id);

          // Validar si ya inicio el curso
          $intento = $this->ultimo_intenoto_curo($id);
          session(['intento_id' =>  $intento['id']]);

          // EXAMEN PRE
          if($this->validar_ya_respondio($id, 'preexamen') == 0){
               $examen = Examen::where('curso_id', '=', $id)->where('tipo', '=',  '2')->first()->toArray();
               return redirect()->route('examen', ['id' => $examen['id']]);
          }

          // Esta en el curso
          if (!is_null($curso) && $this->validar_inscrito_curso(array('curso' => $id, 'status' => 1))) {
               $data = array();
               $data['curso'] = $curso->toArray();
               $data['curso']['examen'] = $this->examen_finalizado($curso['id']);
               $data['curso']['cliente'] = Clientes::find($data['curso']['cliente_id'])->toArray();
               $data['curso']['porcentaje'] = $this->porcentaje_curso($data['curso']['id']);
               $data['secciones'] = $this->get_secciones($id);
               $data['material'] = array('id' => '');
               $data['ultim_curso'] = $this->ultimo_material($id);
               $data['material_introductorio'] = Material_introductorio::select('*')->where('curso_id', $curso['id'])->get()->toArray();
               $data['medicos_participantes'] = Medicos_participantes_cursos::select('*')->where('curso_id', $curso['id'])->get()->toArray();
               $data['recursos_relacionados'] = Recurso_relacionados_curso::select('*')->where('curso_id', $curso['id'])->get()->toArray();
               if($data['ultim_curso'] == 'fin'){
                    $data['examen'] = $this->get_examen_curso($curso['id']);
               }
               $data['ultimo_intento'] = $this->ultimo_intenoto_curo($curso['id']);
               return view('cursos/curso', $data);
          }else{
               return view('errors.404');
          }
     }

     public function get_examen_curso($curso_id){
          return Examen::where('curso_id', $curso_id)->where('tipo', '1')->first();
     }

     public function reiniciar_curso($curso_id){
          $usuario_id = \Auth::user()->id;
          $curso = Cursos::find($curso_id)->toArray();
          $intentos = Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->where('finalizado', '1')->orderBy('id', 'desc')->first();

          if ($intentos) {
               $intentos = $intentos->toArray();
               $intetno = $intentos['num_intento'];
               $intetno += 1;

               if ($curso['num_intentos'] > $intentos['num_intento']) {
                    $intento = new Intentos;
                    $intento->usuario_id = $usuario_id;
                    $intento->examen_id = 0;
                    $intento->inicio = date('Y-m-d H:i:s');
                    $intento->num_intento = $intetno;
                    $intento->curso_id = $curso_id;
                    $intento->save();

                    $material_id = Materiales::select('id')->where('curso_id', $curso_id)->first()->toArray();
                    return redirect('/ver_material/'.$material_id['id']);

               }else{
                    return view('errors.404');
               }
          }else{
               return view('errors.404');
          }
     }


     public function ultimo_intenoto_curo($curso_id){
          $usuario_id = \Auth::user()->id;
          $intentos = Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->where('finalizado', '1')->count();
          $en_curso = Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->whereNull('fin')->count();

          if ($en_curso >= 1) {
               return Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->whereNull('fin')->orderBy('id', 'desc')->first()->toArray();
          }elseif ($intentos >= 1) {
               return Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->where('finalizado', '1')->orderBy('id', 'desc')->first()->toArray();
          }else{
               $intento = new Intentos;
               $intento->usuario_id = $usuario_id;
               $intento->examen_id = 0;
               $intento->inicio = date('Y-m-d H:i:s');
               $intento->num_intento = '1';
               $intento->curso_id = $curso_id;
               $intento->save();
               return $intento->id;
          }
     }

     public function examen_pre($curso_id){
          $examen = Examen::where('curso_id', '=', $curso_id)->where('tipo', '=',  '2')->first()->toArray();

          if ($examen) {
               $respuesta = DB::table('preguntas')->leftJoin('respuestas_alumno', 'respuestas_alumno.pregunta_id', '=', 'preguntas.id')->where('preguntas.examen_id', $examen['id'])->get()->toArray();
               $respuestas  = array_column($respuesta, 'respuesta_id');
               $return  =  true;
               foreach ($respuestas as $key => $respuesta) {
                    if ($respuesta) {
                         $return  = false;
                    }
               }

               return $return;
          }else{
               return true;
          }
     }


     public function validar_ya_respondio($id, $type = 'preexamen'){
          switch ($type) {
               case 'preexamen':
                    $examen = Examen::where('curso_id', '=', $id)->where('tipo', '=',  '2')->first();

                    if ($examen) {
                         $conteo = 0;
                         $preguntas = Preguntas::where('examen_id', $examen->id)->get();
                         foreach ($preguntas as $pregunta_padre) {
                              if ($pregunta_padre->catalogo_preguntas == 3 || $pregunta_padre->catalogo_preguntas == 2 ) {
                                   $preguntas_hijo = Preguntas::where('pregunta_padre', $pregunta_padre->id)->get();
                                   foreach ($preguntas_hijo as $pregunta) {
                                        $info = Respuestas_alumnos::where('pregunta_id', $pregunta->id)->where('intento_id', session('intento_id'));
                                        if ($info) {
                                             $conteo++;
                                        }
                                   }
                              }else{
                                   $info = Respuestas_alumnos::where('pregunta_id', $pregunta_padre->id)->where('intento_id', session('intento_id'));
                                   if ($info) {
                                        $conteo++;
                                   }
                              }

                         }

                         return $conteo;

                         // return DB::table('preguntas')->leftJoin('respuestas_alumno', 'respuestas_alumno.pregunta_id', '=', 'preguntas.id')
                         // ->where('preguntas.examen_id', $examen->toArray()['id'])->where('intento_id', session('intento_id'))->count();
                    }else{
                         return true;
                    }

               break;

          }
          // Examen

     }


     public function ver_material($id){
          $material = Materiales::find($id);


          // Validar si ya inicio el curso
          $intento = $this->ultimo_intenoto_curo($material['curso_id']);
          session(['intento_id' =>  $intento['id']]);

          // EXAMEN PRE
          if($this->validar_ya_respondio($material['curso_id'], 'preexamen') == 0){
               $examen = Examen::where('curso_id', '=', $material['curso_id'])->where('tipo', '=',  '2')->first()->toArray();
               return redirect()->route('examen', ['id' => $examen['id']]);
          }


          if (!is_null($material) && $this->validar_inscrito_curso(array('curso' => $material['curso_id'], 'status' => 1)) && $this->regitrar_material_visto($material)) {

               $data['material'] = $material->toArray();
               $data['secciones'] = $this->get_secciones($data['material']['curso_id']);
               $data['curso']['porcentaje'] = $this->porcentaje_curso($data['material']['curso_id']);
               $data['curso']['info'] = Cursos::find($material['curso_id'])->toArray();


               $preguntas = Preguntas::where('material_id', $id)->get()->toArray();
               foreach ($preguntas as $key => $pregunta) {
                    $data['preguntas'][$key]['pregunta'] = $pregunta;
                    $data['preguntas'][$key]['respuesta'] =  Respuestas::where('pregunta_id',$pregunta['id'])->get()->toArray();
               }

               $data['specialkey'] = array();
               if (!is_null($data['material']['specialkey_id'])) {
                    $data['inf_specialkey'] = Specialkey::where('id', $data['material']['specialkey_id'])->first()->toArray();
                    $preguntas = Preguntas::where('specialkey', $data['material']['specialkey_id'])->get()->toArray();
                    foreach ($preguntas as $key => $pregunta) {
                         $data['specialkey'][$key]['pregunta'] = $pregunta;
                         $data['specialkey'][$key]['respuesta'] =  Respuestas::where('pregunta_id',$pregunta['id'])->get()->toArray();
                    }
               }

               $data['siguiente'] = $this->siguiente($id);
               $data['anterior'] = $this->anterior($id);

               return view('cursos/ver_curso', $data);
          }else{
               return view('errors.404');
          }
     }


     public function siguiente($material_id){
          $material = Materiales::find($material_id)->toArray();
          $curso = $material['curso_id'];
          $seccion = $material['seccion_id'];
          $material_id = $material['id'];

          // Total de secciones
          $max_secciones = Materiales::where('curso_id', $curso)->max('seccion_id');

          // Max material de seccion seleccionada
          $max_materiales =  Materiales::where('seccion_id', $seccion)->orderBy('orden', 'desc')->first()->toArray();

          if ($material_id == $max_materiales['id']) {
               if ($seccion == $max_secciones) {
                    $siguiete = null;
               }else{
                    $siguiete = Materiales::where('seccion_id','>', $seccion)->where('curso_id', $curso)->orderBy('id', 'asc')->first()->toArray();
               }
          }else{
               $siguiete = Materiales::where('seccion_id', $material['seccion_id'])->where('orden','>', $material['orden'])->orderBy('orden', 'asc')->first()->toArray();
          }

          if(!is_null($siguiete))  {
               return $siguiete['id'];
          }else {
               return null;
          }
     }

     public function ultimo_material($curso_id){
          $materiales = Materiales::where('curso_id', $curso_id)->orderBy('seccion_id', 'asc')->orderBy('orden', 'asc')->get()->toArray();
          // $material_anterio
          $iniciado = 'inicio';
          foreach ($materiales as $key => $material) {
               $material_tomado = Material_tomado::where('material_id', $material['id'])->where('intento_id', session('intento_id'))->where('usuario_id', \Auth::user()->id)->first();
               if (!is_null($material_tomado)) {
                    $iniciado = 'fin';
                    if ($material_tomado['finalizado'] == 0) {
                         return array($material['id'], $material['titulo'], 'continuar');
                    }
               }
          }

          if ($materiales) {
               if ($iniciado == 'inicio') {
                    return array($materiales[0]['id'], $materiales[0]['titulo'], 'inicio');
               }else{
                    return $iniciado;
               }
          }else {
               return false;
          }

     }


     public function anterior($material_id)
     {
          $material = Materiales::find($material_id)->toArray();
          $curso = $material['curso_id'];
          $seccion = $material['seccion_id'];
          $material_id = $material['id'];

          // Min de secciones
          $min_secciones = Materiales::where('curso_id', $curso)->min('seccion_id');

          // Min material de seccion seleccionada
          $min_materiales =  Materiales::where('seccion_id', $seccion)->orderBy('orden', 'asc')->first()->toArray();


          if ($material_id == $min_materiales['id']) {
               if ($seccion == $min_secciones) {
                    $anteriro = null;
               }else{
                    $anteriro = Materiales::where('seccion_id','<', $seccion)->where('curso_id', $curso)->orderBy('id', 'desc')->first()->toArray();
               }
          }else{
               $anteriro = Materiales::where('seccion_id', $material['seccion_id'])->where('curso_id', $curso)->where('orden','<', $material['orden'])->orderBy('orden', 'desc')->first()->toArray();
          }

          if(!is_null($anteriro))  {
               return $anteriro['id'];
          }else {
               return null;
          }
     }

     public function get_secciones($curos_id){
          $data = array();
          $secciones = Secciones::where('curso_id', $curos_id)->orderBy('orden', 'asc')->get()->toArray();
          foreach ($secciones as $key => $seccion) {
               $data[$key]['info'] = $seccion;
               $data[$key]['materiales'] = Materiales::where('seccion_id', $seccion['id'])->orderBy('orden', 'asc')->get()->toArray();
          }
          return $data;
     }

     public function finalizar_material_sin_preguntas($material_id){

          $usuario_id = \Auth::user()->id;
          $material = Materiales::find($material_id)->toArray();
          $curso = $material['curso_id'];
          $material_save['finalizado'] = 1;
           Material_tomado::where('usuario_id', $usuario_id)->where('material_id', $material_id)->where('intento_id', session('intento_id'))->update($material_save);
           $siguiente = $this->siguiente($material_id);
           if ($siguiente) {
                return redirect('/ver_material/'.$siguiente);
           }else{
                $mat = Materiales::find($material_id);
                $exa =  Examen::where('curso_id', '=', $mat['curso_id'])->where('tipo', '=',  '1')->first();
                if ($exa['id']) {
                     $intento =  $this->ultimo_intenoto_curo($material['curso_id']);
                    $curso = Cursos::find($curso);

                    if ($curso['num_intentos'] == 0) {
                          return redirect('/examen/'.$exa['id']);
                    }

                    if ($curso['num_intentos'] == $intento['num_intento']) {
                         return redirect('/home');
                    }else{
                         return redirect('/examen/'.$exa['id']);
                    }

                }else{
                     $intento =  $this->ultimo_intenoto_curo($material['curso_id']);
                     $intento['calificacion'] = '100';
                     $intento['finalizado'] = '1';
                     $intento['aprobado'] = '1';
                     $intento['fin'] = date('Y-m-d H:m:s');
                     Intentos::where('id', $intento['id'])->update($intento);
                     return redirect('/home')->with('message', 'has terminado el curso');
                }
           }
     }

     public function fin_material($material_id, Request $request)
     {
          $usuario_id = \Auth::user()->id;

          foreach ($request->all() as $pregunta_id => $respuesta_id) {
               $pregunta = $this->get_respuesta_alumno($pregunta_id, session('intento_id'));
               if ($pregunta) {
                    Respuestas_alumnos::where('pregunta_id', $pregunta_id)
                                             ->where('intento_id', session('intento_id'))
                                             ->update(['respuesta_id' => $respuesta_id]);
               }else{
                    $respuesta = new Respuestas_alumnos();
                    $respuesta->pregunta_id = $pregunta_id;
                    $respuesta->respuesta_id = $respuesta_id;
                    $respuesta->intento_id = session('intento_id');
                    $respuesta->usuario_id = $usuario_id;
                    $respuesta->save();
               }
          }

          $material['finalizado'] = 1;
          Material_tomado::where('usuario_id', $usuario_id)->where('material_id', $material_id)->where('intento_id', session('intento_id'))->update($material);


          $siguiente = $this->siguiente($material_id);
          if ($siguiente) {
               return redirect('/ver_material/'.$siguiente);
          }else{
               $mat = Materiales::find($material_id);
               $exa =  Examen::where('curso_id', '=', $mat['curso_id'])->where('tipo', '=',  '1')->first();
               if ($exa['id']) {
                    return redirect('/examen/'.$exa['id']);
               }else{
                    return redirect('/home')->with('message', 'has terminado el curso');
               }
          }

     }

     public function validar_inscrito_curso($data)
     {
          $usuario_id = \Auth::user()->id;
          switch ($data['status']) {
               // Validar curso
               case '1':
               return Alumnos_cursos::where('usuario_id', $usuario_id)->where('curso_id', $data['curso'])->first();
               break;
          }
     }


     public function regitrar_material_visto($get_material)
     {
          $usuario_id = \Auth::user()->id;
          $curso_tomado = Material_tomado::where('material_id', $get_material['id'])->where('intento_id', session('intento_id'))->where('usuario_id' ,$usuario_id)->first();
          $anterior = $this->anterior($get_material['id']);
          $curso_anterio = Material_tomado::where('material_id', $anterior)->where('usuario_id' ,$usuario_id)->first();
          if (is_null($anterior)) {
               if (is_null($curso_tomado)) {
                    $material = new Material_tomado();
                    $material->usuario_id = $usuario_id;
                    $material->material_id = $get_material['id'];
                    $material->intento_id = session('intento_id');
                    $material->finalizado = 0;
                    $material->save();
                    return true;
               }else {
                    return true;
               }
          }else {
               $curso_anterio = Material_tomado::where('material_id', $anterior)->where('usuario_id' ,$usuario_id)->first();
               if (is_null($curso_anterio)) {
                    $material = new Material_tomado();
                    $material->usuario_id = $usuario_id;
                    $material->material_id = $get_material['id'];
                    $material->intento_id = session('intento_id');
                    $material->finalizado = 0;
                    $material->save();
                    return true;
               }else if($curso_anterio['finalizado'] == 1){
                    if (is_null($curso_tomado)) {
                         $material = new Material_tomado();
                         $material->usuario_id = $usuario_id;
                         $material->material_id = $get_material['id'];
                         $material->intento_id = session('intento_id');
                         $material->finalizado = 0;
                         $material->save();
                         return true;
                    }else{
                         return true;
                    }
               }else{
                    return false;
               }
          }
     }

     public function save_especialkey($material_id, Request $request)
     {
          $usuario_id = \Auth::user()->id;
          if (isset($request['pregunta'])) {
               if (count($request['pregunta']) > 0) {
                    foreach ($request['pregunta'] as $pregunta => $respuesta) {
                         $pregunta = $this->get_respuesta_alumno($pregunta_id, session('intento_id'));
                         if ($pregunta) {
                              Respuestas_alumnos::where('pregunta_id', $pregunta_id)
                                                       ->where('intento_id', session('intento_id'))
                                                       ->update(['respuesta_id' => $respuesta_id]);
                         }else{
                              $resp = new Respuestas_alumnos();
                              $resp->pregunta_id = $pregunta;
                              $resp->respuesta_id = $respuesta;
                              $resp->usuario_id = $usuario_id;
                              $resp->intento_id = session('intento_id');
                              $resp->save();
                         }
                    }
               }
          }

          $material['finalizado'] = 1;
          Material_tomado::where('usuario_id', $usuario_id)->where('material_id', $material_id)->update($material);

          $siguiente = $this->siguiente($material_id);

          if ($siguiente) {
               return redirect('/ver_material/'.$siguiente);
          }else{
               return redirect('/home')->with('message', 'has terminado el curso');
          }
     }

     public function get_respuesta_alumno($pregunta_id, $intento_id){
          return Respuestas_alumnos::where('pregunta_id', $pregunta_id)->where('intento_id', $intento_id)->first();
     }

     public function terminar_curso_sin_examen($material_id){

          $siguiente = $this->siguiente($material_id);
          $material = Materiales::find($material_id)->toArray();
          if (!$siguiente) {
               $existe_examen_final = Examen::where('curso_id', '=', $material['curso_id'])->where('tipo', '=',  '1')->first();
               if (!$existe_examen_final) {
                    $intento =  $this->ultimo_intenoto_curo($material['curso_id']);
                    $intento['calificacion'] = '100';
                    $intento['finalizado'] = '1';
                    $intento['aprobado'] = '1';
                    $intento['fin'] = date('Y-m-d H:m:s');
                    Intentos::where('id', $intento['id'])->update($intento);

                    return redirect('home')->with('message', 'Felicitaciones por haber terminado el curso.');
               }else{
                    return redirect('home')->with('message', 'Felicitaciones por haber terminado el curso');
               }
          }else{
               return redirect('/ver_material/'.$siguiente);
          }
     }


}
