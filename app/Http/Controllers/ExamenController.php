<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Examen;
use App\Preguntas;
use App\Respuestas;
use App\Respuestas_alumnos;
use App\Respuestas_preguntas;
use App\Intentos;
use App\Cursos;
use App\Materiales;


use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFundation\Response;

// require_once('mpdf/Mpdf.php');



class ExamenController extends Controller
{

    public function examen($examen_id, $instrucciones = false){
          $data['examen'] = Examen::where('id', $examen_id)->first()->toArray();

          if ($data['examen']['instrucciones']  != '' && $instrucciones == false) {
               return view('examen.instrucciones', $data);
          }

         $data['examen']['tiempo'] = $this->convert_time_seconds( $data['examen']['tiempo']) ?  $this->convert_time_seconds( $data['examen']['tiempo']) : false;
         $data['preguntas'] = array();
         $preguntas = Preguntas::where('examen_id', $data['examen']['id'])->get()->toArray();
          foreach ($preguntas as $key => $pregunta) {
               if ($pregunta['catalogo_preguntas'] == 1 || $pregunta['catalogo_preguntas'] == 4 || $pregunta['catalogo_preguntas'] == 5 || $pregunta['catalogo_preguntas'] == 6 ) {
                    $data['preguntas'][$key]['pregunta'] = $pregunta;
                    $data['preguntas'][$key]['respuesta'] =  Respuestas::where('pregunta_id',$pregunta['id'])->orderBy('opcion', 'asc')->get()->toArray();
               }

               if ($pregunta['catalogo_preguntas'] == 2 || $pregunta['catalogo_preguntas'] == 3) {
                    $data['preguntas'][$key]['pregunta'] = $pregunta;
                    $data['preguntas'][$key]['pregunta']['preguntas_hijo'] = Preguntas::where('pregunta_padre', $pregunta['id'])->get()->toArray();
                    foreach ($data['preguntas'][$key]['pregunta']['preguntas_hijo'] as $key1 => $pregunta) {
                             $data['preguntas'][$key]['pregunta']['preguntas_hijo'][$key1]['respuestas'] = Respuestas::where('pregunta_id',$pregunta['id'])->orderBy('opcion', 'asc')->get()->toArray();
                    }
               }
         }

          return view('examen.form', $data);
    }




    public function convert_time_seconds($time)
    {
         $parsed = date_parse($time);
         return $parsed['hour'] * 3600 + $parsed['minute'] * 60 + $parsed['second'];
    }

    public function get_respuesta_alumno($pregunta_id, $intento_id){
            return Respuestas_alumnos::where('pregunta_id', $pregunta_id)->where('intento_id', $intento_id)->first();
      }

     public function calificar_examen($examen_id, Request $request)
     {
          $examen = Examen::where('id', $examen_id)->first()->toArray();
          $usuario_id = \Auth::user()->id;
          $fecha_inicio = $request['f_inicio'];


          $intento = Intentos::where('curso_id', $examen['curso_id'])->where('usuario_id', $usuario_id)->orderby('id','DESC')->first();
          session(['intento_id' =>  $intento['id']]);


          if (isset($request['pregunta'])) {
               if (count($request['pregunta']) > 0) {
                    foreach ($request['pregunta'] as $pregunta_id => $respuesta) {
                         $pregunta = $this->get_respuesta_alumno($pregunta_id, session('intento_id'));
                         $respuesta = $respuesta != "" ? (int)$respuesta : 0;
                         if ($pregunta) {
                              $res = Respuestas_alumnos::where('id',$pregunta->id)->delete();
                         }
                         $resp = new Respuestas_alumnos();
                         $resp->pregunta_id = $pregunta_id;
                         $resp->respuesta_id = $respuesta;
                         $resp->usuario_id = $usuario_id;
                         $resp->intento_id = session('intento_id');
                         $resp->save();
                    }
               }
          }



          // session(['calificacion' => json_encode($this->get_calificacion($examen_id))]);


          if ($examen['tipo'] == 2) {
               return redirect('/respuetas_cliente/'. $examen_id);
          }


          $aprobado = $this->obtener_calificacion(session('intento_id'), $examen_id);

          if($aprobado){
               return redirect('respuetas_cliente/'. $examen_id)->with('message', 'Felicitaciones por haber aprobado');
          }else{
               return redirect('respuetas_cliente/'. $examen_id)->with('error', 'Intentalo de nuevo, has reprobado');
          }
     }



     public function respuetas_cliente($examen){
          $data['examen'] = Examen::where('id', $examen)->first()->toArray();
          $preguntas = Preguntas::where('examen_id', $examen)->get()->toArray();
          $curso = Cursos::find($data['examen']['curso_id'])->toArray();

          $usuario_id = \Auth::user()->id;
          $ultimo_intento = Intentos::where('curso_id', $curso['id'])->where('usuario_id', $usuario_id)->orderby('id','DESC')->first();


          foreach ($preguntas as $key => $pregunta) {
               if ($pregunta['catalogo_preguntas'] == 1 || $pregunta['catalogo_preguntas'] == 4 || $pregunta['catalogo_preguntas'] == 5 || $pregunta['catalogo_preguntas'] == 6 ) {
                    $data['preguntas'][$key]['pregunta'] = $pregunta;
                    $data['preguntas'][$key]['respuesta'] =  Respuestas::where('pregunta_id',$pregunta['id'])->orderBy('opcion', 'asc')->get()->toArray();
                    $data['preguntas'][$key]['respuesta_alumno'] = Respuestas_alumnos::where('pregunta_id',$pregunta['id'])->where('usuario_id',$usuario_id)->where('intento_id',$ultimo_intento->id)->first();
               }

               if ($pregunta['catalogo_preguntas'] == 2 || $pregunta['catalogo_preguntas'] == 3) {
                    $data['preguntas'][$key]['pregunta'] = $pregunta;
                    $data['preguntas'][$key]['pregunta']['preguntas_hijo'] = Preguntas::where('pregunta_padre', $pregunta['id'])->get()->toArray();
                    foreach ($data['preguntas'][$key]['pregunta']['preguntas_hijo'] as $key1 => $pregunta_hijo) {

                             $data['preguntas'][$key]['pregunta']['preguntas_hijo'][$key1]['respuestas'] = Respuestas::where('pregunta_id',$pregunta_hijo['id'])->orderBy('opcion', 'asc')->get()->toArray();



                              $data['preguntas'][$key]['pregunta']['preguntas_hijo'][$key1]['respuestas_alumno'] = Respuestas_alumnos::where('pregunta_id',$pregunta_hijo['id'])->where('usuario_id',$usuario_id)->where('intento_id',$ultimo_intento->id)->first();
                    }
               }
         }

          return view('examen/respuestas', $data);
     }

     public function get_calificacion($examen_id){
          $usuario_id = \Auth::user()->id;
          $preuntas_padre =  DB::table('preguntas')->where('examen_id', $examen_id)->get();
          $examen = Examen::where('id', $examen_id)->first()->toArray();
          $curso = Cursos::find($examen['curso_id'])->toArray();
          $ultimo_intento = Intentos::where('curso_id',$curso['id'])->where('usuario_id', $usuario_id)->orderby('id','DESC')->first();
          $buenas = 0;
          $malas = 0;
          $array_buenas = array();
          $array_malas = array();

          $repuestas = array();

          foreach ($preuntas_padre as $key => $pregunta) {
               switch ($pregunta->catalogo_preguntas) {
                    case 1:
                    case 4:
                    case 5:
                    case 6:
                    case 0:
                         $repuseta_correcta = Respuestas::where('pregunta_id',$pregunta->id)->where('correcta', '1')->first();
                         if ($repuseta_correcta->id) {
                              $respuesta_alumno = Respuestas_alumnos::where('pregunta_id',$pregunta->id)->where('respuesta_id',$repuseta_correcta->id)->where('usuario_id',$usuario_id)->where('intento_id',$ultimo_intento->id)->first();
                              $repuestas = $respuesta_alumno;
                              if ($respuesta_alumno) {
                                   array_push($array_buenas, $pregunta->titulo);
                                   $buenas++;
                              }else{
                                   array_push($array_malas, $pregunta->titulo);
                                   $malas++;
                              }
                         }
                    break;
                    case '2':
                         $pregunta_hijos = DB::table('preguntas')->where('pregunta_padre', $pregunta->id)->get();
                         if ($pregunta_hijos) {
                              foreach ($pregunta_hijos as $key => $pregunta_hijo) {
                                   $repuseta_correcta = Respuestas::where('pregunta_id',$pregunta_hijo->id)->where('correcta', '1')->first();
                                   if ($repuseta_correcta->id) {
                                        $respuesta_alumno = Respuestas_alumnos::where('pregunta_id',$pregunta_hijo->id)->where('respuesta_id',$repuseta_correcta->id)->where('usuario_id',$usuario_id)->where('intento_id',$ultimo_intento->id)->first();

                                        $respuesta_alumno_error = Respuestas_alumnos::leftJoin('respuestas_preguntas', 'respuestas_preguntas.id', '=', 'respuestas_alumno.respuesta_id')->where('respuestas_alumno.pregunta_id',$pregunta_hijo->id)->where('respuestas_alumno.usuario_id',$usuario_id)->where('respuestas_alumno.intento_id',$ultimo_intento['id'])->first();

                                        $repuestas = $respuesta_alumno_error;

                                        if ($respuesta_alumno) {
                                             array_push($array_malas, strip_tags($pregunta->descripcion).' ('.$pregunta_hijo->titulo .' => '. $respuesta_alumno_error->opcion.')');
                                             $buenas++;
                                        }else{
                                             array_push($array_malas, strip_tags($pregunta->descripcion).' ('.$pregunta_hijo->titulo .' => '. $respuesta_alumno_error->opcion.')');
                                             $malas++;
                                        }
                                   }
                              }
                         }
                    break;
                    case '3':
                    $pregunta_hijos = DB::table('preguntas')->where('pregunta_padre', $pregunta->id)->get();
                    if ($pregunta_hijos) {
                         foreach ($pregunta_hijos as $key => $pregunta_hijo) {
                              $repuseta_correcta = Respuestas::where('pregunta_id',$pregunta_hijo->id)->where('correcta', '1')->first();
                              if ($repuseta_correcta->id) {
                                   $respuesta_alumno = Respuestas_alumnos::where('pregunta_id',$pregunta_hijo->id)->where('respuesta_id',$repuseta_correcta->id)->where('usuario_id',$usuario_id)->where('intento_id',$ultimo_intento->id)->first();

                                   $repuestas = $respuesta_alumno;


                                   if ($respuesta_alumno) {
                                        array_push($array_buenas, $pregunta->descripcion.' '. $pregunta_hijo->titulo);
                                        $buenas++;
                                   }else{
                                        array_push($array_malas, $pregunta->titulo.' '. $pregunta_hijo->titulo);
                                        $malas++;
                                   }
                              }
                         }
                    }
                    break;

               }
          }

          return  [$buenas, $malas, $array_buenas, $array_malas, $repuestas];

     }


     /*
          1. RESPUESTAS PRE EXAMEN
          2. RESPUESTAS CASO CLINICO
          3. RESPUESTAS MODULOS
          4. RESPUESTAS POST EXAMEN
     */
     public function respuestas_type($id, $type){
          switch ($type) {
               case '1':
                    return DB::table('preguntas')
                         ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                         ->where('respuestas_preguntas.correcta', '1')
                         ->where('preguntas.examen_id', $id)->get()->toArray();
               break;
               case '2':
                    return DB::table('preguntas')
                              ->leftJoin('materiales', 'materiales.specialkey_id', '=', 'preguntas.specialkey')
                              ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                              ->where('respuestas_preguntas.correcta', '1')
                              ->where('materiales.curso_id', $id)
                              ->get()->toArray();
               break;
               case '3':
                    return DB::table('preguntas')
                         ->leftJoin('materiales', 'materiales.id', '=', 'preguntas.material_id')
                         ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                         ->where('respuestas_preguntas.correcta', '1')
                         ->where('materiales.curso_id', $id)
                         ->get()->toArray();

               break;
               case '4':
                    return DB::table('preguntas')
                         ->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')
                         ->where('respuestas_preguntas.correcta', '1')
                         ->where('preguntas.examen_id', $examen_id)->get()->toArray();
               break;
          }
     }



     public function obtener_calificacion($intento_id, $examen_id)
     {

          $info_examen = Examen::where('id', $examen_id)->first()->toArray();
          $info_curso = Cursos::find($info_examen['curso_id'])->toArray();


          // Obtener preguntas con respuesta correctas
          $respuestas =  DB::table('preguntas')->leftJoin('respuestas_preguntas', 'respuestas_preguntas.pregunta_id', '=', 'preguntas.id')->where('respuestas_preguntas.correcta', '1')->where('preguntas.examen_id', $examen_id)->get()->toArray();

          // Obtener respuestas del usuario
          $respuestas_usuario = Respuestas_alumnos::where('intento_id', $intento_id)->get()->toArray();

          // contador respuestas correctas
          $conteo = 0;
          $conteo_a_calificar = 0;
          $sum_total = 0;
          // contador de preguntas
          $conteo_pregntas = count($respuestas);


          // respuestas validar_pre_examen L
          if ($info_curso['calificar_pre_examen']) {
               $conteo_a_calificar++;
               $new_examen = Examen::where('curso_id', '=', $info_curso['id'])->where('tipo', '=',  '2')->first()->toArray();
               $respuestas_alumno = $this->get_calificacion($new_examen['id']);
               $count_preguntas = array_sum($respuestas_alumno);
               $count_respuestas = $respuestas_alumno[0];
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;
          }


          // respuestas caso clinico
          if ($info_curso['calificar_caso_clinico']) {
               $conteo_a_calificar++;
               $respuestas = $this->respuestas_type($info_curso['id'], '2');
               $respuestas_cor  = array_column($respuestas, 'id');
               $respuestas_correctas = Respuestas_alumnos::whereIn('respuesta_id', $respuestas_cor)->where('intento_id', $intento_id)->get()->toArray();
               $count_preguntas = count($respuestas);
               $count_respuestas = count($respuestas_correctas);
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;
          }

          // respuestas post modulo L
          if ($info_curso['calificar_preguntas_modulo']) {
               $conteo_a_calificar++;
               $respuestas = $this->respuestas_type($info_curso['id'], '3');
               $respuestas_cor = array_column($respuestas, 'id');
               $respuestas_correctas = Respuestas_alumnos::whereIn('respuesta_id', $respuestas_cor)->where('intento_id', $intento_id)->get()->toArray();
               $count_preguntas = count($respuestas_cor);
               $count_respuestas = count($respuestas_correctas);
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;
          }

          // respuesta port_examen
          if ($info_curso['calificar_post_examen']) {
               $conteo_a_calificar++;
               $new_examen = Examen::where('curso_id', '=', $info_curso['id'])->where('tipo', '=',  '1')->first()->toArray();
               $respuestas = $this->respuestas_type($new_examen['id'], '1');
               $respuestas_alumno = $this->get_calificacion($new_examen['id']);
               $count_preguntas = array_sum($respuestas_alumno);
               $count_respuestas = $respuestas_alumno[0];
               $sum_total +=  ($count_respuestas * 100) / $count_preguntas;
          }


          // obtener informacion de examen
          $examen =  Examen::where('id', $examen_id)->first()->toArray();

          // porcentaje para aprobar el examen
          $aprobar = $info_curso['calificacion_aprovatoria'];


          // promedio obtenido
          $total =  number_format(($sum_total  / $conteo_a_calificar), 2);

          $intento['aprobado'] = 0;
          $intento['calificacion'] = $total;
          if ($aprobar <= $total) {
               $intento['aprobado'] = 1;
          }
          $intento['finalizado'] = 1;
          $intento['fin'] = date('Y-m-d H:i:s');

          Intentos::where('id', $intento_id)->update($intento);
          return $intento['aprobado'];
     }


     public function certificado($curso_id){

          $usuario_id = \Auth::user()->id;
          $intentos = Intentos::where('curso_id', $curso_id)->where('usuario_id', $usuario_id)->where('aprobado', '1')->first();

          if ($intentos['id']) {
               $url = 'https://capapei.com/certificado/pdf.php?intento_id='.$intentos['id'];
               header('Location: '.$url.'');
          }else{
               return redirect('home/dashboard');
          }

     }


}
