<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuarios;
use App\Especialidades;
use App\Alumnos;
use App\Pais;
use App\Residentes;
use App\Instituciones;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFundation\Response;



class AlumnosController extends Controller
{

     public function editar( $id = null)
     {
          $data['id'] = isset($id) ? $id : \Auth::user()->id ;
          $data['especialidades'] = Especialidades::orderByDesc('id')->get()->toArray();
          $data['pais'] = Pais::orderBy('nombre')->get()->toArray();
          $data['residentes'] = Residentes::orderBy('nombre')->get()->toArray();
          $data['instituciones'] = Instituciones::orderBy('nombre')->get()->toArray();
          $data['usuario'] = DB::table('alumnos')->leftJoin('usuarios', 'alumnos.usuario_id', '=', 'usuarios.id')->where('usuarios.id', $data['id'] )->first();

          return view('alumnos/editar', $data);
     }

     public function editar_data_perfil($id, Request $request)
     {

          $Validar = $this->validate($request, [
              'nombre' => 'required|max:255',
              'apellidos' => 'required|max:255',
          ]);

          if ($request['password']) {
               $Validar = $this->validate($request, [ 'password' => 'required|min:6|confirmed']);
          }

          
          // Modifciar Usuario
          $usuarios = array();
          if (isset($request->nombre)){ $usuarios['nombre'] = $request->nombre; }
          if (isset($request->apellido_paterno)){ $usuarios['apellido_paterno'] = $request->apellido_paterno;  }
          if (isset($request->apellidos_materno)){ $usuarios['apellidos_materno'] = $request->apellidos_materno;  }
          if (isset($request->sexo)){ $usuarios['sexo'] = $request->sexo;  }
          if (isset($request->contrasena)) { $usuarios['contrasena'] = bcrypt($request->contrasena); }
          if (count($usuarios) > 0) {
               $affectedRows = Usuarios::where('id', $id)->update($usuarios);
          }

          //Modifciar Alumno
          $alumno = array();
          if (isset($request->especialidad_id)){ $alumno['especialidad_id'] = $request->especialidad_id; }
          if (isset($request->telefono)){ $alumno['telefono'] = $request->telefono; }
          if (isset($request->celular)){ $alumno['celular'] = $request->celular; }
          if (isset($request->cedula_profecional)){ $alumno['celular'] = $request->cedula_profecional; }
          if (isset($request->edad)){ $alumno['celular'] = $request->edad; }
          if (isset($request->pais)){ $alumno['celular'] = $request->pais; }
          if (isset($request->ciudad)){ $alumno['celular'] = $request->ciudad; }
          if (isset($request->codigo_postal)){ $alumno['celular'] = $request->codigo_postal; }
          if (isset($request->institucion)){ $alumno['celular'] = $request->institucion; }
          if (count($usuarios) > 0) {
               $affectedRows = Alumnos::where('usuario_id', $id)->update($alumno);
          }

     }


     public function registro(Request $request)
     {
          $data['curso_id'] = $request->curso_id;
          $data['correo'] = $request->correo;
          $data['especialidades'] = Especialidades::orderByDesc('id')->get()->toArray();
          return view('auth.register', $data );
          // return view('alumnos.editar', $data );
     }


}
