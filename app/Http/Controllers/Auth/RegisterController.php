<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Usuarios;
use App\Alumnos;
use App\Especialidades;
use App\Alumnos_cursos;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {

        return Validator::make($data, [
            'nombre' => 'required|max:255',
            'apellido_paterno' => 'required|max:255',
            'apellidos_materno' => 'required|max:255',
            'sexo' => 'required|max:255',
            'edad' => 'required|max:255',
            'pais' => 'required|max:255',
            'cedula_profecional' => 'required|max:255',
            'especialidad' => 'required|max:255',
            'institucion' => 'required|max:255',
            'celular' => 'required|max:255',
            'correo' => 'required|email|max:255|unique:usuarios|confirmed',
            'password' => 'required|min:6|confirmed',
            'terminos' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {



        // return Usuarios::create([
        $usuario =  Usuarios::create([
            'nombre' => $data['nombre'],
            'apellido_paterno' => $data['apellido_paterno'],
            'apellidos_materno' => $data['apellidos_materno'],
            'sexo' => $data['sexo'],
            'correo' => $data['correo'],
            'password' => bcrypt($data['password']),
            'rol_id' => 1,
            'estatus' => 0,
        ]);


        $alumno = new Alumnos();
        $alumno->usuario_id = $usuario->id;
        $alumno->especialidad_id = $data['especialidad'];
        $alumno->celular = $data['celular'];
        $alumno->cedula = $data['cedula_profecional'];
        $alumno->edad = $data['edad'];
        $alumno->pais_id = $data['pais'];
        $alumno->ciudad = $data['ciudad'];
        $alumno->cp = $data['codigo_postal'];
        $alumno->institucion_id = $data['institucion'];
        $alumno->term_cond = $data['terminos'];
        $alumno->save();

        return $usuario;
    }

     public function registro(Request $request)
     {

          $data['curso_id'] = $request->curso_id;
          $data['correo'] = $request->correo;
          $data['especialidades'] = Especialidades::orderByDesc('id')->get()->toArray();
          return view('auth.register', $data );
          // return view('alumnos.editar', $data );

     }
}
