<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Catalogos;


class PaginaController extends Controller{

     public function index(){
            return view('pagina/pagina', []);
     }

     public function registor_exito(){
          return view('pagina/registro_exito', []);
     }

     public function exito_registro(){
          return view('pagina/exito_registro', []);
     }


     public function insertar(){

     }
}
