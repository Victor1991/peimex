<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Materiales;
use App\Material_tomado;

class Cursos extends Model
{
     protected $table = 'cursos';


     public function materiales()
     {
          return $this->belongsTo('App\Materiales', 'id', 'curso_id');
     }

}
