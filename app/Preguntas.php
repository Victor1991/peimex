<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas extends Model
{
     protected $table = 'preguntas';

     public function respuestas()
     {
          return $this->belongsTo('App\Respuestas', 'id', 'pregunta_id');
     }

}
