<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Material_introductorio extends Model
{
     const CREATED_AT = NULL;
     const UPDATED_AT = NULL;

     protected $table = 'material_introductorio';
}
