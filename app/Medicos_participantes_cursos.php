<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicos_participantes_cursos extends Model
{
     const CREATED_AT = NULL;
     const UPDATED_AT = NULL;

     protected $table = 'medicos_participantes_cursos';
}
