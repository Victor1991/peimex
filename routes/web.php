<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Login
Auth::routes();

// Ver formulario Login
Route::get('login/{url?}', 'Auth\LoginController@showLoginForm')->name("login");


//pagina
Route::get('/', 'PaginaController@index')->name("pagina");

// Registro exitos
Route::get('registro_exitoso', 'PaginaController@registor_exito')->name("registro_exitoso");


Route::post('validar_correo', 'UsuariosController@validar_correo')->name("validar_correo");


Route::get('insertar', 'PaginaController@insertar')->name("insertar");


// Ver formulario de registro
// Route::get('registro', 'AlumnosController@registro')->name('registro');


// Rutas con acceso al logearse
Route::group( ['middleware' => 'auth' ], function(){

     // ------- Cursos -------

     // Incio
     Route::get('/home/{id?}', 'CursosController@index');

     // Cursos
     // Route::get('/', 'CursosController@index')->name("main");

     // Descripcion del curso
     Route::get('/curso/{id}', 'CursosController@curso')->name("curso");

     // Ver contenido curso
     Route::get('/ver_material/{id}', 'CursosController@ver_material')->name("ver_material");

     // Ver curso anterior
     Route::get('/siguiente/{id}', 'CursosController@siguiente')->name("siguiente");

     // Ver siquiente curso
     Route::get('/anterior/{id}', 'CursosController@anterior')->name("anterior");

     // Finalizar material
     Route::any('/fin_material/{id}', 'CursosController@fin_material')->name("fin_material");

     // Finalizar material sin preguntas
     Route::any('/fin_material_sin_preguntas/{id}', 'CursosController@finalizar_material_sin_preguntas')->name("fin_material_sin_preguntas");

     Route::get('/terminar_sin_examen/{id}', 'CursosController@terminar_curso_sin_examen')->name('terminar_sin_examen');

     // Examen
     Route::get('/examen/{id}/{instrucciones?}', 'ExamenController@examen')->name("examen");

     // Pre Examen
     Route::get('/pre_examen/{id}/{instrucciones?}', 'ExamenController@examen')->name("examen");


     // Califiacar examen
     Route::any('/calificar_examen/{id}', 'ExamenController@calificar_examen')->name('calificar_examen');

     // Obtener certificado
     Route::get('/certificado/{id}', 'ExamenController@certificado')->name('certificado');

     //Text obtener calificacion
     Route::get('/get_calificacion/{id}', 'ExamenController@get_calificacion')->name('get_calificacion');

     //Vista ver respuestas
     Route::get('/respuetas_cliente/{id}', 'ExamenController@respuetas_cliente')->name('respuetas_cliente');


     Route::get('/mpdf', 'ExamenController@mpdf')->name('mpdf');

     Route::post('/save_especialkey/{id}', 'CursosController@save_especialkey')->name('save_especialkey');

     //Cerrar sesion
     Route::get('/logout', function(){
        Auth::logout();
        return Redirect::to('/');
     });

     // Editar ver form alumno
     Route::get('/editar_perfil/{id?}', 'AlumnosController@editar')->name("editar_alumno");

     // Editar update DB
     Route::post('/editar_data_perfil/{id}', 'AlumnosController@editar_data_perfil')->name("editar_data_perfil");


     // Reiniciar curso
     Route::get('/reiniciar_curso/{id}', 'CursosController@reiniciar_curso')->name("reiniciar_curso");



});
