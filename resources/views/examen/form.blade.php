@extends('layouts.app')

@section('title', 'Main page')

@section('content')

<!-- Page heading -->
<!--<div class="row wrapper border-bottom bg-info page-heading ">
<div class="container m-t-md">
<div class="col-md-12">
<h1>Evalución</h1>
</div>
</div>
</div>-->


<div class="container m-t-md ">

     <!-- Cursos -->
     <div class="col-md-8">
          <h2>{{ $examen['titulo'] }}</h2>
          <h3>{{ $examen['descripcion'] }}</h3>
          <br>
          <input type="hidden" id="vista_examen" value="examen">
          <div class="ibox">
               <div class="ibox-content">
                    <form  name="crono" id="regForm" method="post" action="{{ route('calificar_examen', ['id' => $examen['id']]) }}">
                         <input type="hidden" name="f_inicio" value="{{ date('Y-m-d h:i:s') }}"/>
                         @foreach ($preguntas as $key => $pregunta)

                         <?php if ($pregunta['pregunta']['catalogo_preguntas'] == 1 || $pregunta['pregunta']['catalogo_preguntas'] == 4 || $pregunta['pregunta']['catalogo_preguntas'] == 5 || $pregunta['pregunta']['catalogo_preguntas'] == 6 ): ?>
                              <div class="tab">
                                   {!! $pregunta['pregunta']['descripcion'] !!}
                                   <h4>{{ $pregunta['pregunta']['titulo'] }}</h4>
                                   <br/>
                                   <div class="row">
                                        @foreach ($pregunta['respuesta'] as $key2 => $respuestas)

                                        <div class="col-md-12 ">
                                             <label class="container">
                                                  <p style="margin-left:16px; font-size: 15px; text-align: justify;">
                                                       <?=$respuestas['opcion']?>
                                                  </p>
                                                  <?php if($pregunta['pregunta']['catalogo_preguntas'] == 5): ?>
                                                       <input type="checkbox" class="resp_especialkey" mensaje="{{ $respuestas['descripcion'] }}" pregunta="{{ $pregunta['pregunta']['id'] }}"  name="pregunta[{{ $pregunta['pregunta']['id'] }}]" value="{{ $respuestas['id'] }}" >
                                                  <?php else: ?>
                                                       <input type="radio" class="resp_especialkey" mensaje="{{ $respuestas['descripcion'] }}" pregunta="{{ $pregunta['pregunta']['id'] }}"  name="pregunta[{{ $pregunta['pregunta']['id'] }}]" value="{{ $respuestas['id'] }}" >
                                                  <?php endif; ?>
                                                  <span class="checkmark"></span>
                                             </label>
                                        </div>
                                        @endforeach
                                   </div>
                              </div>
                         <?php endif; ?>
                         <?php if ($pregunta['pregunta']['catalogo_preguntas'] == 2): ?>
                              <?php $texto =  $pregunta['pregunta']['descripcion']; ?>
                              <?php foreach ($pregunta['pregunta']['preguntas_hijo'] as $key => $pregunta_hijo): ?>
                                   <?php
                                   $select = '<select name="pregunta['.$pregunta_hijo['id'].']">';
                                   foreach ($pregunta_hijo['respuestas'] as $key => $respuesta) {
                                        $select .= '<option value="'.$respuesta['id'].'">'.$respuesta['opcion'].'</option>';
                                   }
                                   $select .= '</select>';
                                   $texto =  str_replace(  $pregunta_hijo['titulo'], $select, $texto);
                                   ?>
                              <?php endforeach; ?>
                              <div class="tab">
                                   <label class="container">
                                        <?php echo $texto; ?>
                                   </label>
                              </div>
                         <?php endif; ?>


                         <?php if ($pregunta['pregunta']['catalogo_preguntas'] == 3): ?>
                              <div class="tab">
                                   <div class="content">
                                        <?php
                                        $let = array('a','b','c','d','e','f','g','h','i','j');
                                        $respu = array();
                                        $pregun = array();
                                        foreach ($pregunta['pregunta']['preguntas_hijo'] as $key => $value) {
                                             $pregun[] = array(
                                                  'id' => $value['id'],
                                                  'titulo' => $value['titulo']
                                             );
                                             foreach ($value['respuestas'] as $key => $respuesta) {
                                                  $respu[] = array(
                                                       'id' => $respuesta['id'],
                                                       'opcion' => $respuesta['opcion']
                                                  );
                                             }
                                        }
                                        shuffle($respu);
                                        shuffle($pregun);
                                        ?>
                                        <table class="table">
                                             <thead>
                                                  <tr>
                                                       <th>Pregunta</th>
                                                       <th>Opcion</th>
                                                       <th>Repuesta</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php for ($i=0; $i < count($respu) ; $i++) { ?>
                                                       <tr>
                                                            <td><?=$pregun[$i]['titulo'];?></td>
                                                            <td>
                                                                 <select class="form-control" name="pregunta[{{ $pregun[$i]['id'] }}]">
                                                                      <option value="" >-- Selecciona una respuesta </option>
                                                                      @foreach ($respu as $k => $r)
                                                                           <option value="{{ $r['id'] }}"> {{  $let[$k] }} </option>
                                                                      @endforeach
                                                                 </select>
                                                            </td>
                                                            <td><?='('.$let[$i].') '. $respu[$i]['opcion'] ?> </td>
                                                       </tr>

                                                  <?php } ?>

                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                         <?php endif; ?>
                         @endforeach
                         <br/>
                         <div>
                              <div style="float:right;">
                                   <button type="button" id="prevBtn"  onclick="nextPrev(-1)">Anterior</button>
                                   <button type="button" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                              </div>
                         </div>
                         <!-- Circles which indicates the steps of the form: -->
                         <div style="display:none;">
                              <button type="submit" id="btn_submit" >hola</button>
                         </div>

                    </form>
               </div>
          </div>
     </div>

     <!-- Temporalizadors  -->
     <div class="col-md-4" >
          <div class="ibox">
               <div class="ibox-content" style="text-align: center;">
                    <?php if($examen['tiempo']): ?>
                         <h2>Tiempo restante</h2>
                         <script>
                         function alerta() {
                              $('#myModal').modal('show');
                              window.setInterval( 'submit()' , 5000);
                         }
                         function submit() {
                              $( "#regForm" ).submit()
                         }
                         var myCountdown2 = new Countdown({time: {{ $examen['tiempo'] }},
                         width:200,
                         height:80,
                         hideLine: 1,
                         rangeHi:"hour",	// <- no comma on last item!
                         numbers : {
                              font : "Arial",
                              color : "#FFFFFF",
                              bkgd	: "#005a95",
                              rounded : 0.15,
                              shadow : {
                                   x : 0,
                                   y : 3,
                                   s : 4,
                                   c : "#000000",
                                   a : 0.4
                              }
                         },
                         labels : {
                              font   : "Arial",
                              color  : "#a8a8a8",
                              textScale 	: 0.8,
                              weight : "normal" // < - no comma on last item!
                         },
                         labelText	: {
                              ms    	: "MS",
                              second : "SEGUNDOS",
                              minute : "MINUTOS",
                              hour  	: "HORAS"
                         },
                         onComplete : alerta
                    });


                    </script>
                    <style> div#Stage_jbeeb_3{  margin: 0 auto !important; } </style>
               <?php endif;?>
               <div style="text-align:center;margin-top:40px;">
                    @foreach ($preguntas as $key => $pregunta)
                    <span class="step">{{ $key +1 }}</span>
                    @endforeach
               </div>
          </div>
     </div>
</div>
</div>



<div id="myModal" class="modal fade" data-backdrop="static" role="dialog">
     <div class="modal-dialog modal-sm">

          <!-- Modal content-->
          <div class="modal-content">
               <div class="modal-body" style="text-align: center;">
                    <div class="row">
                         <img src="https://i.pinimg.com/originals/7a/05/1f/7a051f327179adcd3d5d1c19644aa7f2.png" alt="reloj" style="height:200px; width:200px;">
                         <h2>El timpo se ha terminado</h2>
                    </div>
               </div>
          </div>

     </div>
</div>

@endsection
