@extends('layouts.app')

@section('title', 'Main page')

@section('content')


<div class="container m-t-md ">

     <!-- Cursos -->
     <div class="col-md-12">
          <h2>{{ $examen['titulo'] }}</h2>
          <h3>{{ $examen['descripcion'] }}</h3>
          <br>
          <div class="container m-t-md">
               @if(session('message'))
               <div class="alert alert-success alert-dismissible" style="background-color: #0a548d; border-color: #0a548d; color: #fff;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> Exito !</strong> {{ session('message') }}
               </div>
               @endif

               @if(session('error'))
               <div class="alert alert-danger alert-dismissible" >
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong> Error !</strong> {{ session('error') }}
               </div>
               @endif
          </div>
          <br>
          <div class="ibox">
               <div class="ibox-content">
                    <div >
                         @foreach ($preguntas as $key => $pregunta)

                         <?php if ($pregunta['pregunta']['catalogo_preguntas'] == 1 || $pregunta['pregunta']['catalogo_preguntas'] == 4 || $pregunta['pregunta']['catalogo_preguntas'] == 5 || $pregunta['pregunta']['catalogo_preguntas'] == 6 ): ?>
                              <div >
                                   {!! $pregunta['pregunta']['descripcion'] !!}
                                   <h4 style="background-color: #cacaca; padding: 10px; border-radius: 5px; color: #fff;">{{ $pregunta['pregunta']['titulo'] }}</h4>
                                   <br/>
                                   <div class="row">
                                        @foreach ($pregunta['respuesta'] as $key2 => $respuestas)
                                        <div class="col-md-12 ">
                                             <label class="container
                                                  <?php if ($pregunta['respuesta_alumno'] != null) :?>
                                                            <?php if ($pregunta['respuesta_alumno']->respuesta_id  == $respuestas['id'] && $respuestas['correcta'] == 1 ): ?>
                                                                 text-success
                                                            <?php endif; ?>
                                                            <?php if ($pregunta['respuesta_alumno']->respuesta_id  == $respuestas['id'] && $respuestas['correcta'] == 0 ): ?>
                                                                 text-danger
                                                            <?php endif; ?>
                                                  <?php endif; ?>">
                                                  <p style="margin-left:16px; font-size: 15px; text-align: justify;">
                                                       <?=$respuestas['opcion']?>
                                                  </p>
                                                  <?php if($pregunta['pregunta']['catalogo_preguntas'] == 5): ?>
                                                       <input disabled
                                                            type="checkbox"
                                                            class="resp_especialkey"
                                                            mensaje="{{ $respuestas['descripcion'] }}"
                                                            pregunta="{{ $pregunta['pregunta']['id'] }}"
                                                            name="pregunta[{{ $pregunta['pregunta']['id'] }}]"
                                                            value="{{ $respuestas['id'] }}"
                                                            <?php if ($pregunta['respuesta_alumno'] != null) :?>
                                                                      <?php if ($pregunta['respuesta_alumno']->respuesta_id  == $respuestas['id']): ?>
                                                                           checked
                                                                      <?php endif; ?>
                                                            <?php endif; ?>
                                                       >
                                                  <?php else: ?>
                                                       <input disabled
                                                            type="radio"
                                                            class="resp_especialkey"
                                                            mensaje="{{ $respuestas['descripcion'] }}"
                                                            pregunta="{{ $pregunta['pregunta']['id'] }}"
                                                            name="pregunta[{{ $pregunta['pregunta']['id'] }}]"
                                                            value="{{ $respuestas['id'] }}"
                                                            <?php if ($pregunta['respuesta_alumno'] != null) :?>
                                                                      <?php if ($pregunta['respuesta_alumno']->respuesta_id  == $respuestas['id']): ?>
                                                                           checked
                                                                      <?php endif; ?>
                                                            <?php endif; ?>
                                                       >
                                                  <?php endif; ?>
                                                  <span class="checkmark"></span>
                                             </label>
                                        </div>
                                        @endforeach
                                   </div>
                              </div>
                         <?php endif; ?>
                         <?php if ($pregunta['pregunta']['catalogo_preguntas'] == 2): ?>
                              <?php $texto =  $pregunta['pregunta']['descripcion']; ?>
                              <?php foreach ($pregunta['pregunta']['preguntas_hijo'] as $key => $pregunta_hijo): ?>
                                        <?php
                                                  $keys = array_search($pregunta_hijo['respuestas_alumno']['respuesta_id'], array_column($pregunta_hijo['respuestas'], 'id'));
                                                  $res_alum = $pregunta_hijo['respuestas'][$keys];
                                                  $class =  $res_alum['correcta'] == 1 ? 'bac_succces' : 'bac_danger';
                                        ?>
                                   <?php
                                   $select = '<select class="'.$class.'"  name="pregunta['.$pregunta_hijo['id'].']">';
                                   foreach ($pregunta_hijo['respuestas'] as $key => $respuesta) {
                                        $check = $res_alum['id'] == $respuesta['id'] ? 'selected' : '';
                                        $select .= '<option value="'.$respuesta['id'].'"  '.$check.'  >'.$respuesta['opcion'].'</option>';
                                   }
                                   $select .= '</select>';
                                   $texto =  str_replace(  $pregunta_hijo['titulo'], $select, $texto);
                                   ?>
                              <?php endforeach; ?>
                              <div style="border: 1px solid #cacaca; padding: 10px; border-radius: 5px;" >
                                   <label class="container">
                                        <?php echo $texto; ?>
                                   </label>
                              </div>
                              <br>
                         <?php endif; ?>


                         <?php if ($pregunta['pregunta']['catalogo_preguntas'] == 3): ?>
                              <div style="border: 1px solid #cacaca; padding: 10px; border-radius: 5px;">
                                   <div class="content">
                                        <?php
                                        $let = array('a','b','c','d','e','f','g','h','i','j');
                                        $respu = array();
                                        $pregun = array();
                                        foreach ($pregunta['pregunta']['preguntas_hijo'] as $key => $value) {
                                             $pregun[] = array(
                                                  'id' => $value['id'],
                                                  'titulo' => $value['titulo']
                                             );
                                             foreach ($value['respuestas'] as $key => $respuesta) {
                                                  $respu[] = array(
                                                       'id' => $respuesta['id'],
                                                       'opcion' => $respuesta['opcion']
                                                  );
                                             }
                                        }
                                        ?>
                                        <table class="table">
                                             <thead>
                                                  <tr>
                                                       <th>Pregunta</th>
                                                       <th>Opcion</th>
                                                       <th>Repuesta</th>
                                                  </tr>
                                             </thead>
                                             <tbody>
                                                  <?php for ($i=0; $i < count($respu) ; $i++) { ?>
                                                       <tr
                                                            <?php if ($pregunta['pregunta']['preguntas_hijo'][$key]['respuestas_alumno'] == $pregunta['pregunta']['preguntas_hijo'][$key]): ?>
                                                            <?php endif; ?>
                                                       >
                                                            <td><?=$pregun[$i]['titulo'];?></td>
                                                            <td>
                                                                 <?php
                                                                      $class =  $pregunta['pregunta']['preguntas_hijo'][$i]['respuestas_alumno']['respuesta_id'] == $respu[$i]['id'] ? 'bac_succces' : 'bac_danger';
                                                                 ?>
                                                                 <select class="form-control {{$class}}" name="" disabled>
                                                                      <option value="" >-- Selecciona una respuesta </option>
                                                                      @foreach ($respu as $k => $r)

                                                                           <option
                                                                                value="{{ $r['id'] }}"
                                                                                {{ $pregunta['pregunta']['preguntas_hijo'][$i]['respuestas_alumno']['respuesta_id'] == $r['id'] ? 'selected' : ''  }}
                                                                           > {{  $let[$k] }} </option>
                                                                      @endforeach
                                                                 </select>

                                                            </td>
                                                            <td><?='('.$let[$i].') '. $respu[$i]['opcion'] ?> </td>
                                                       </tr>

                                                  <?php } ?>

                                             </tbody>
                                        </table>
                                   </div>
                              </div>
                              <br>
                         <?php endif; ?>
                         @endforeach
                         <br/>
                         <div>
                              <div style="float:right;">
                                   <?php if($examen['tipo'] == 1): ?>
                                        <a class="button button-border button-border-thin button-blue" href="/sistema/home"> Salir al inicio</a>
                                   <?php else: ?>
                                        <a class="button button-border button-border-thin button-blue" href="{{ url('/curso/'.$examen['curso_id']) }}"> Continuar con el curso</a>
                                   <?php endif; ?>

                              </div>
                         </div>

                    </div>
               </div>
          </div>
     </div>
</div>
</div>

<style>
     .text-success{
          color: #3fcd41 !important
     }
</style>

@endsection
