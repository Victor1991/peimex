@extends('layouts.app')

@section('title', 'Main page')

@section('content')

<!-- Page heading -->

<div class="container m-t-md ">
     <div class="fancy-title title-bottom-border">
          <h3>Instrucciones del examen  <span><?=$examen['titulo']?></span> </h3>
     </div>
     <!-- Cursos -->
     <div class="row">
          <div class="col-md-12">
               <?=$examen['instrucciones']?>
          </div>
          <div class="col-md-12">
               <div class="divider"><i class="icon-info"></i></div>
          </div>
          <div class="col-md-12">
               <a  href="{{ url('/pre_examen/'.$examen['id'].'/1') }}" class="button button-rounded button-reveal button-large button-border tright pull-right button-green"><i class="icon-line-arrow-right"></i><span>Realizar examen</span></a>
               <a href="{{ url('/home') }}" class="button button-rounded button-reveal button-large button-border tright button-blue"><i class="icon-reply"></i><span>Regresar</span></a>


          </div>
     </div>
</div>

@endsection
