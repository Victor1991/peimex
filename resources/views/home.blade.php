@extends('layouts.app')
@section('title', 'Main page')
@section('content')



<!-- Page heading -->
<div class="jumbotron" style=" background-image: url({!! asset('public/pagina/img/bg121.png') !!});  padding-top:40px;padding-bottom:40px;background-repeat:no-repeat;background-position:center;background-size:cover;-webkit-background-size:cover; border-radius: 0px;" >
     <div class="row">
          <div class="col-md-6 text-center">

               <span class="tit1">Bienvenido a</span><br>
               <img src="{!! asset('public/pagina/img/logo2.png') !!}">
               <p style="font-size: 28px; color: #fff;">CapaPEI</p>
          </div>
     </div>
</div>



<?php
     if(session('calificacion')):
     $preg = json_decode(session('calificacion'));
?>
<script>
     window.addEventListener("load", function(){
          $('#califaciones').modal('show');
     });
</script>

<div class="modal fade" id="califaciones" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog">
          <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Resultado de la evalución</h4>
               </div>
               <div class="modal-body">
                    <div class="row">
                         <div class="col-md-6 text-success text-center">
                              <h2 style="color:#3c763d">
                                   Correctas
                              </h2>

                              <?php foreach ($preg[2] as $key => $value): ?>
                                   <div class="col-md-12 respues">
                                        <?=$value ?>
                                   </div>
                                   <br><br>
                              <?php endforeach; ?>

                         </div>
                         <div class="col-md-6 text-warning text-center">
                              <h2 style="color:#a94442">
                                   Incorrectas
                              </h2>

                              <?php foreach ($preg[3]  as $key => $value): ?>
                              <div class="col-md-12 respues">
                                   <?=$value ?>
                              </div>
                              <br><br>
                              <?php endforeach; ?>

                         </div>
                    </div>
               </div>
               <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

               </div>
          </div>
     </div>
</div>

<?php
     Session::forget('calificacion');
     endif;
?>

<div class="container m-t-md">
     @if(session('message'))
     <div class="alert alert-success alert-dismissible" style="background-color: #0a548d; border-color: #0a548d; color: #fff;">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> Exito !</strong> {{ session('message') }}
     </div>
     @endif

     @if(session('error'))
     <div class="alert alert-danger alert-dismissible" >
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong> Error !</strong> {{ session('error') }}
     </div>
     @endif
</div>

<section id="content">
     <div class="container clearfix">
          <div class="tabs tabs-bb clearfix" id="tab-9" style="border: 0px;">

               <ul class="tab-nav clearfix">
                    <li><a href="#tabs-33">MIS CURSOS</a></li>

                    <!-- <li ><a href="#tabs-36">Mis constancias</a></li> -->
               </ul>
               <br>
               <div class="row">
                    <div class="col-md-12" style="text-align: right;">
                         <a href="{{ url('/home/lista') }}" class="button button-border button-border-thin button-blue"
                         <?php if ($type == 'lista'): ?> disabled="disabled" <?php endif; ?>
                         ><i class="icon-th-list"></i>Lista</a>
                         <a href="{{ url('/home/') }}" class="button button-border button-border-thin button-blue"
                         <?php if ($type == 'mosaico'): ?> disabled="disabled" <?php endif; ?>
                         ><i class="icon-th"></i>Mosaico</a>
                    </div>
               </div>

               <div class="tab-container" style="border: 0px;">
                    <div class="clearfix" id="tabs-33" style="border: 0px;">
                         <?php if ($type == 'mosaico'): ?>
                              <div id="oc-portfolio" class="owl-carousel portfolio-carousel carousel-widget" data-margin="20" data-nav="true" data-pagi="false" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-md="3" data-items-lg="3">

                                   @foreach($cursos as $curso)
                                   <?php if ($curso['estatus'] == '3'): ?>
                                        <div class="oc-item">
                                             <div class="iportfolio">
                                                  <div class="portfolio-image " >
                                                       <?php if ($curso['tipo_archivo'] == 'video'): ?>
                                                            <div class="" style="height: 300px; background-color:#6aaaea; border-radius: 30px;">
                                                                 <video src="https://capapei.com/material/principal/{!! $curso['imagen'] !!}" controls="controls" controlslist="nodownload" class="card-img" style="height: 100% !important; object-fit: cover; border-radius: 30px;"></video>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                       <?php else: ?>
                                                            <img src="https://capapei.com/material/principal/{!! $curso['imagen'] !!}" class="img-rounded " style="height:300px;"  >
                                                       <?php endif; ?>

                                                       <div class="text-overlay img-rounded" style="">
                                                            <div>
                                                                 <h4 class="" style="color: #fff; font-weight: bold; margin: 0px;" >
                                                                      {{ $curso['num_modulos']}}
                                                                      @if( $curso['num_modulos'] > 1 )
                                                                           MÓDULOS
                                                                      @else
                                                                           MÓDULO
                                                                      @endif
                                                                 </h4>
                                                                 <p style="color: #fff; font-size: 18px; line-height: 20px; font-weight: 100; margin-bottom: 0px;">{{ $curso['nombre']}}<br>
                                                                      <?php if ( !is_null( $curso['curos_caduca']) || $curso['curos_caduca'] != '' ): ?>
                                                                           <?php if ($curso['curos_caduca'] != '0000-00-00'): ?>
                                                                                Caduca el {{ date("d/m/Y", strtotime($curso['curos_caduca'])) }}<br>
                                                                           <?php endif; ?>
                                                                      <?php endif; ?>
                                                                      <?php if ( isset( $curso['examen'] )): ?>
                                                                           Num. preguntas examen ({{ $curso['examen']['n_preg'] }})</p>
                                                                      <?php endif; ?>
                                                                 </p>
                                                            </div>
                                                       </div>
                                                  </div>



                                                  <div class="portfolio-desc" style="margin-right: 0px">

                                                       <div class="row">
                                                            <div class="col-xs-6">
                                                                 @if($curso['ultimo_material'][2] == 'inicio' && $curso['aprobado'] != 1)

                                                                 <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Iniciar curso</a>

                                                                 @elseif($curso['ultimo_material'][2] == 'continuar')

                                                                 <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Continuar</a>

                                                                 @elseif($curso['aprobado'] == 1)

                                                                 <p class="text-info">Felicidades por aprobar el curso</p>
                                                                 <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="_blank" class="btn btn-sm btn-outline btn-success btn-block">Ver tu certificado</a>

                                                                 @else

                                                                 <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-green btn-sm">Ver curso</a>

                                                                 <?php if ($curso['ultimo_intento']) : ?>
                                                                      <?php if ($curso['ultimo_intento']['aprobado'] == 1): ?>
                                                                           <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="-_blank" class="button  button-small button-circle button-border button-teal btn-sm">Certificado </a>

                                                                      <?php endif; ?>

                                                                 <?php endif; ?>


                                                                 <?php if (is_null($curso['ultimo_intento']['fin'])) : ?>
                                                                      <?php if ($curso['examen']['id']): ?>


                                                                           <a type="button" href="{{ url('/examen/'.$curso['examen']['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Evaluación </a>
                                                                      <?php endif; ?>
                                                                 <?php endif;?>


                                                                 @endif

                                                            </div>

                                                            <div class="col-xs-6" style=" text-align: right;">
                                                                 <?php if ($curso['porcentaje'][0]): ?>
                                                                      <span style="font-size: 13px; margin-top:11px;"><?=$curso['porcentaje'][0]?>% completado</span>
                                                                 <?php else: ?>
                                                                      <span style="font-size: 13px; margin-top:11px;">0 % completado</span>
                                                                 <?php endif; ?>

                                                                 <?php if ($curso['ultimo_intento']) : ?>
                                                                      <?php if ($curso['ultimo_intento']['aprobado'] == 1): ?>
                                                                           <span style="font-size: 18px; margin-top:11px;">
                                                                                {{  date("d-m-Y", strtotime($curso['ultimo_intento']['fin'])) }}
                                                                           </span>
                                                                      <?php endif; ?>
                                                                 <?php endif; ?>
                                                            </div>
                                                       </div>

                                                  </div>


                                             </div>
                                        </div>
                                   <?php endif; ?>

                                   @endforeach


                              </div>
                         <?php else: ?>
                              @foreach($cursos as $curso)
                              <?php if ($curso['estatus'] == '3'): ?>
                                   <div class="list-group">
                                        <div href="#" class="list-group-item list-group-item-action flex-column align-items-start">
                                             <div class="col-md-3">

                                                  <?php if ($curso['tipo_archivo'] == 'video'): ?>
                                                       <div class="" style="height: 200px; background-color:#6aaaea; border-radius: 30px;">
                                                            <video src="https://capapei.com/material/principal/{!! $curso['imagen'] !!}" controls="controls" controlslist="nodownload" class="card-img" style="height: 100% !important; object-fit: cover; border-radius: 30px;"></video>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                  <?php else: ?>
                                                       <img src="https://capapei.com/material/principal/{!! $curso['imagen'] !!}" class="img-rounded " style="height:200px;"  >
                                                  <?php endif; ?>
                                             </div>
                                             <div class="col-md-9">
                                                  <div class="d-flex w-100 justify-content-between">
                                                       <h4 class="" style="color: #000; font-weight: bold; margin: 0px;" >
                                                            {{ $curso['num_modulos']}}
                                                            @if( $curso['num_modulos'] > 1 )
                                                                 MÓDULOS
                                                            @else
                                                                 MÓDULO
                                                            @endif
                                                       </h4>
                                                       <p style="color: #000; font-size: 18px; line-height: 30px; font-weight: 100; margin-bottom: 0px;">{{ $curso['nombre']}}<br>
                                                            <?php if ( !is_null( $curso['curos_caduca']) || $curso['curos_caduca'] != '' ): ?>
                                                                 <?php if ($curso['curos_caduca'] != '0000-00-00'): ?>
                                                                      Caduca el {{ date("d/m/Y", strtotime($curso['curos_caduca'])) }}<br>
                                                                 <?php endif; ?>
                                                            <?php endif; ?>
                                                            <?php if ( isset( $curso['examen'] )): ?>
                                                                 Num. preguntas examen ({{ $curso['examen']['n_preg'] }})</p>
                                                            <?php endif; ?>
                                                       </p>
                                                  </div>
                                                  <p class="mb-1" style="margin-bottom: 0px; ">
                                                       @if($curso['ultimo_material'][2] == 'inicio')

                                                       <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Iniciar curso</a>

                                                       @elseif($curso['ultimo_material'][2] == 'continuar')

                                                       <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Continuar</a>

                                                       @elseif($curso['aprobado'] == 1)

                                                       <p class="text-info">Felicidades por aprobar el curso</p>
                                                       <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="_blank" class="btn btn-sm btn-outline btn-success btn-block">Ver tu certificado</a>

                                                       @else

                                                       <a type="button" href="{{ url('/curso/'.$curso['id']) }}" class="button  button-small button-circle button-border button-green btn-sm">Ver curso</a>

                                                       <?php if ($curso['ultimo_intento']) : ?>
                                                            <?php if ($curso['ultimo_intento']['aprobado'] == 1): ?>
                                                                 <a type="button" href="{{ url('/certificado/'.$curso['id']) }}" target="-_blank" class="button  button-small button-circle button-border button-teal btn-sm">Certificado </a>

                                                            <?php endif; ?>

                                                       <?php endif; ?>


                                                       <?php if (is_null($curso['ultimo_intento']['fin'])) : ?>
                                                            <?php if ($curso['examen']['id']): ?>
                                                                 <a type="button" href="{{ url('/examen/'.$curso['examen']['id']) }}" class="button  button-small button-circle button-border button-aqua btn-sm">Exámen </a>
                                                            <?php endif; ?>
                                                       <?php endif;?>


                                                       @endif
                                                       <br>
                                                       <?php if ($curso['porcentaje'][0]): ?>
                                                            <span style="font-size: 13px; margin-top:11px;"><?=$curso['porcentaje'][0]?>% completado</span>
                                                       <?php else: ?>
                                                            <span style="font-size: 13px; margin-top:11px;">0 % completado</span>
                                                       <?php endif; ?>
                                                  </p>
                                             </div>
                                             <div class="clearfix">

                                             </div>
                                        </div>
                                   </div>
                              <?php endif; ?>

                              @endforeach

                         <?php endif; ?>

                    </div>

                    <!-- <div class="tab-content clearfix" id="tabs-36"  style="border: 0px;">
                    Próximamente
               </div> -->

          </div>

     </div>
</div>
</section>

@endsection
