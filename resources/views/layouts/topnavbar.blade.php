

<header id="header">

			<div id="header-wrap">

				<div class="container clearfix">

					<div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

					<!-- Logo
					============================================= -->
					<div id="logo">
						<a href="" class="standard-logo" data-dark-logo="{!! asset('img/be_logo.png') !!}"><img src="{!! asset('public/img/be_logo.png') !!}" alt=""></a>
						<a href="" class="retina-logo" data-dark-logo="{!! asset('img/be_logo.png') !!}"><img src="{!! asset('public/img/be_logo.png') !!}" alt=""></a>
					</div><!-- #logo end -->

					<!-- Primary Navigation
					============================================= -->
					<nav id="primary-menu" class="style-2">

						<ul>
							<li class="dropdown">

								 <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">

									 {{ Auth::user()->nombre }}  {{ Auth::user()->apellido_paterno }}  {{ Auth::user()->apellido_materno }}<img src="{!! asset('public/img/logo_perfil.png') !!}" class="img-perfil" alt="perfil">
								 </a>
								 <ul class="dropdown-menu dropdown-alerts">
									<!-- <li>
										 <a href="{{ route('editar_alumno') }}">
											 Editar perfil
										 </a>
									 </li>-->
									 <li>
										 <a href="{{ route('logout') }}"
												onclick="event.preventDefault();
													    document.getElementById('logout-form').submit();">
											 Cerrar sesion
										 </a>
									 </li>
									 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
										 {{ csrf_field() }}
									 </form>
								 </ul>
							 </li>
                                     <li><a href="{{ url('/home') }}" style="color: #676a6c; font-weight: bold"><div> MIS CURSOS</div></a></li>

						</ul>



					</nav><!-- #primary-menu end -->

				</div>

			</div>

		</header><!-- #header end -->
