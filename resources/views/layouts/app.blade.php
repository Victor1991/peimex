<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Peimex') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{!! asset('public/css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('public/css/app.css') !!}" />



        <link rel="stylesheet" href="{!! asset('public/css/bootstrap.css') !!}" type="text/css" />
	<link rel="stylesheet" href="{!! asset('public/css/style.css') !!}" type="text/css" />
	<link rel="stylesheet" href="{!! asset('public/css/swiper.css') !!}" type="text/css" />

	<link rel="stylesheet" href="{!! asset('public/css/font-icons.css') !!}" type="text/css" />
	<link rel="stylesheet" href="{!! asset('public/css/animate.css') !!}" type="text/css" />

        <link rel="stylesheet" href="{!! asset('public/css/magnific-popup.css') !!}" type="text/css" />

	<link rel="stylesheet" href="{!! asset('public/css/responsive.css') !!}" type="text/css" />
        <link rel="stylesheet" href="{!! asset('public/css/estilos/estilos.css') !!}" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />





    <!-- Sweet alert  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css" />


    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>

    <script src="{!! asset('public/js/countdown.js') !!}" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfobject/2.1.1/pdfobject.js" type="text/javascript"></script>

</head>
<body class="stretched">

  <!-- Wrapper-->
    <div id="wrapper" class="clearfix">

        <!-- Navigation -->
        <!-- @include('layouts.navigation') -->

        <!-- Page wraper -->


            <!-- Page wrapper -->
            @include('layouts.topnavbar')


            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('layouts.footer')


        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->

    <script src="{!! asset('public/js/app.js') !!}" type="text/javascript"></script>

    <!-- Sweet alert  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js" type="text/javascript"></script>


     <!-- JS ADD -->
     <script src="{!! asset('public/js/script.js') !!}" type="text/javascript"></script>


        <script type="text/javascript" src="{!! asset('public/js/jquery.js') !!}"></script>
	<script type="text/javascript" src="{!! asset('public/js/plugins.js') !!}"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="{!! asset('public/js/functions.js') !!}"></script>


     </script>

@section('scripts')
@show

</body>
</html>
