<!DOCTYPE html>
<html>

<head>

     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">

     <title>Certificado </title>

     <link href="css/bootstrap.min.css" rel="stylesheet">
     <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

     <!-- Toastr style -->
     <!-- Styles -->
     <link rel="stylesheet" href="{!! asset('css/vendor.css') !!}" />
     <link rel="stylesheet" href="{!! asset('css/app.css') !!}" />
</head>

<body>
     <div id="wrapper">
          <div class="row">
               <div class="wrapper wrapper-content  animated fadeInRight article">
                    <div class="row justify-content-md-center">
                         <div class="col-md-offset-4 col-md-4">
                              <div class="ibox">
                                   <div class="ibox-content" >
                                        <div class="col-md-12">
                                             <div class="text-center">
                                                  <h1>
                                                       <img src="{!! asset('img/be_logo.png') !!}" alt="logo" class="img-responsive" style=" margin: 0 auto; width:300px;">
                                                  </h1>
                                                  <p>otorga este certificado a </p>
                                             </div>

                                             <div class="row text-center">
                                                  <div class="m-">

                                                       <H2> {{ $usuario->nombre. ' ' . $usuario->apellido_paterno. ' ' . $usuario->apellidos_materno  }}</H2>

                                                  </div>
                                                  <div class="m-xl">
                                                       <p>por haber superado con éxito el curso </p>
                                                       <H2> {{ $curso['nombre'] }} </H2>
                                                  </div>
                                             </div>


                                        </div>

                                        <div class="clearfix">

                                        </div>
                                   </div>
                              </div>
                         </div>
                    </div>
               </div>
          </div>
     </div>

     <style media="screen">
     .ibox-content{
          /* Permalink - use to edit and share this gradient: http://colorzilla.com/gradient-editor/#e4f5fc+0,bfe8f9+14,9fd8ef+37,2ab0ed+100 */
          background: rgb(228,245,252); /* Old browsers */
          background: -moz-linear-gradient(-45deg, rgba(250,250, 250,1) 70%, rgba(191,232,249,.5) 14%, rgba(159,216,239,1) 37%, rgba(42,176,237,1) 100%); /* FF3.6-15 */
          background: -webkit-linear-gradient(-45deg, rgba(250,250, 250,1) 70%,rgba(191,232,249,.5) 14%,rgba(159,216,239,1) 37%,rgba(42,176,237,1) 100%); /* Chrome10-25,Safari5.1-6 */
          background: linear-gradient(135deg, rgba(250,250, 250,1) 70%,rgba(191,232,249,.5) 14%,rgba(159,216,239,1) 37%,rgba(42,176,237,1) 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
          filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e4f5fc', endColorstr='#2ab0ed',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
     }
     </style>
</body>
</html>
