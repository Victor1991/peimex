@extends('layouts.app')

@section('title', 'Main page')

@section('content')

<!-- Page heading -->



<style>
    .head_curso{
        <?php if ($curso['imagen_small']): ?>
               background: url('http://peimexlearning.com/material//principal/818ae2ef1e4c861887dfa84891ec51bc.png') no-repeat center center fixed;
          <?php else: ?>
            background: url('http://peimexlearning.com/material//principal/818ae2ef1e4c861887dfa84891ec51bc.png') no-repeat center center fixed;
        <?php endif; ?>

         -webkit-background-size: cover;
         -moz-background-size: cover;
         -o-background-size: cover;
         background-size: cover;
    }
</style>

 <?php
     if(session('calificacion')):
     $preg = json_decode(session('calificacion'));
 ?>

<script>
     window.addEventListener("load", function(){
          $('#califaciones').modal('show');
     });
</script>

<div class="modal fade" id="califaciones" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
     <div class="modal-dialog modal-lg">
          <div class="modal-content">
               <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="">Resultado de la evalución</h4>
               </div>
               <div class="modal-body">
                    <div class="row">
                         <div class="col-md-6 text-success text-center">
                              <h2 style="color:#3c763d">
                                   Correctas
                              </h2>

                              <?php foreach ($preg[2] as $key => $value): ?>
                                   <div class="col-md-12 respues">
                                        <?=$value ?>
                                   </div>
                                   <br><br>
                              <?php endforeach; ?>

                         </div>
                         <div class="col-md-6 text-warning text-center">
                              <h2 style="color:#a94442">
                                   Incorrectas
                              </h2>

                              <?php foreach ($preg[3]  as $key => $value): ?>
                              <div class="col-md-12 respues">
                                   <?=$value ?>
                              </div>
                              <br><br>
                              <?php endforeach; ?>

                         </div>
                    </div>
               </div>
               <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

               </div>
          </div>
     </div>
</div>

<?php
     Session::forget('calificacion');
     endif;
?>

<section id="content">
     <div class="container clearfix">
          <br>
          <div class="row">
               <div class="col-md-6">
                    <a href="{{ url('/home') }}" class="button button-mini button-dark button-rounded"><i class="icon-reload"></i>Regresar</a>
               </div>
               <div class="col-md-6" style="text-align: right; margin-top: 20px;">
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item"><a href="{{ url('/home') }}">Cursos</a></li>
                         <li class="breadcrumb-item active" aria-current="page">{{ $curso['nombre'] }}</li>
                    </ol>
               </div>
          </div>

          <hr>

        <div class="jumbotron "
               style="
                    <?php if ($curso['imagen_small']): ?>
                         background-image: url('http://capapei.com/material/principal/{{$curso['imagen_small']}}');
                    <?php else: ?>
                         background-image: url('http://capapei.com/material/principal/b_sin_imagen.png');
                    <?php endif; ?>
                    padding-top:70px;
                    padding-bottom:40px;
                    background-repeat:no-repeat;
                    background-position:center;
                    background-size:cover;
                    -webkit-background-size:cover;
                    border-radius: 10px;" >
            <div class="row">
                <div class="col-md-12">

                    <span class="tit1" style="color:#ffffff">{{ $curso['nombre'] }}</span><br>

                 <p style="font-size: 16px; color: #ffffff; text-align: justify;"><?php echo $curso['descripcion'] ?></p>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div>
                    <span style="color: #8d8d8d; font-size: 16px;">Completado: </span>
                    <small class="pull-right" style="color: #8d8d8d; font-size: 16px;">{{ $curso['porcentaje'][2]}} Módulos de  {{ $curso['porcentaje'][1]}}</small>
                </div>
                <div class="progress progress-small">
                    <div style="width: {{ $curso['porcentaje'][0] }}%;" class="progress-bar"></div>
                </div>
            </div>

        </div>



        <div class="row">
            <div class="col-lg-12">






                <div class="tabs tabs-bb clearfix" id="tab-9" style="border: 0px;">

                    <ul class="tab-nav clearfix">
                        <li><a href="#tabs-33">Descripción</a></li>
                         <?php if($curso['instrucciones'] != ''){ ?>
                              <li><a href="#tabs-34">Instrucciones</a></li>
                         <?php } ?>
                        <li><a href="#tabs-35">Módulos</a></li>


                        <?php if($curso['instrucciones'] > 0){ ?>
                        <li ><a href="#tabs-38">Encuesta</a></li>

                        <?php } ?>


                        <li ><a href="#tabs-36">Instructores</a></li>
                        <?php if( count($recursos_relacionados) > 0){?>
                        <li ><a href="#tabs-37">Materiales de apoyo</a></li>
                        <?php } ?>
                    </ul>

                    <div class="tab-container" style="border: 0px; margin-top: 25px;">

                        <div class="clearfix" id="tabs-33" style="border: 0px;">


                            @foreach ($material_introductorio as $key_mat => $material_intro)
                                 <div class="row">
                                     <div class="col-md-6">
                                        <?php if($material_intro['tipo_archivo'] == 'video'): ?>
                                              <video poster="images/videos/{{ $material_intro['archivo'] }}" controls controlsList="nodownload" preload="auto" controls style="display: block; width: 100%;">
                                                  <source src='http://capapei.com/material/principal/{{ $material_intro['archivo'] }}' type='video/mp4' />
                                              </video>
                                        <?php else: ?>
                                             <img src='http://capapei.com/material/principal/{{ $material_intro['archivo'] }}' alt="" class="img-responsive img-thumbnail">
                                        <?php endif; ?>

                                     </div>
                                     <div class="col-md-6">
                                         <h3 style=" margin: 0 0 3px 0;">{!! $material_intro['titulo'] !!}</h3>
                                         <h4  style="color: #000 !important; font-weight: 100">{!! $material_intro['descripcion'] !!}</h4>

                                     </div>
                                 </div>
                                   <br>
                              @endforeach



                               <?php
                               echo $curso['objetivo'];
                               ?>


                        </div>
                        <div class="tab-content clearfix" id="tabs-34"  style="border: 0px;">

                              {!! $curso['instrucciones'] !!}






                        </div>
                        <div class="tab-content clearfix" id="tabs-35"  style="border: 0px;     min-height: 1120px;">
                            @include('cursos.secciones')
                        </div>



                        <?php if($curso['instrucciones'] > 0){ ?>
                        <div class="tab-content clearfix" id="tabs-38"  style="border: 0px;">
                            <H3  style="color: #556c98;  font-weight: 200; margin: 0 0 20px 0;">Encuesta</h3>
                        </div>
                        <?php } ?>


                        <div class="tab-content clearfix" id="tabs-36"  style="border: 0px;">




                            <div class="row">
                                   @foreach ($medicos_participantes as $key_mat => $medico)
                                          <div class="col-md-6 col-lg-6">
                                              <div class="slide" style=" margin-bottom: 30px;">
                                                  <div class="testi-image">
                                                      <img src="http://capapei.com/material/profesores/{!!$medico['foto']!!}" alt="">
                                                  </div>
                                                  <div class="testi-content">
                                                      <p style="line-height: 1.2;">
                                                         {{ $medico['nombre'] }}
                                                      </p>
                                                      <div class="testi-meta">

                                                          <span>

                                                      <?php echo $medico['descripcion']; ?>
                                                          </span>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>

                                   @endforeach

                            </div>



                        </div>



                         <?php if( count($recursos_relacionados) > 0){?>



                        <div class="tab-content clearfix" id="tabs-37"  style="border: 0px;">


                            <H3  style="color: #556c98;  font-weight: 200; margin: 0 0 20px 0;">Materiales de apoyo</h3>





                            <div class="row">




                              @foreach ($recursos_relacionados as $key_mat => $rec_rel)
                                     <div class="col-md-12 col-lg-12">

                                          <div class="style-msg2" style="background-color: #EEE;">
                                               <div class="msgtitle">{{ $rec_rel['tipo_archivo'] }}</div>
                                               <div class="sb-msg">
                                                    <?php if ($rec_rel['tipo_archivo'] == 'video'): ?>
                                                         <video poster="images/videos/explore.jpg" preload="auto" controls style="display: block; width: 100%;height:500px;">
                                                              <source src="http://capapei.com/material/video/{{ $rec_rel['archivo']  }}" type='video/mp4' />
                                                         </video>
                                                    <?php  elseif ($rec_rel['tipo_archivo'] == 'pdf'): ?>
                                                         <iframe v-if="form.tipo_archivo == 'pdf'" src="https://docs.google.com/viewer?url=http://capapei.com/material/video/{{ $rec_rel['archivo']  }}&embedded=true" style="width:100%; height:500px;"></iframe>

                                                    <?php else: ?>
                                                         <iframe v-if="form.tipo_archivo == 'office'" src="https://view.officeapps.live.com/op/view.aspx?src=http://capapei.com/material/video/{{ $rec_rel['archivo'] }}" style="width:100%; height:500px;"></iframe>

                                                    <?php endif; ?>
                                                    <h5 style=" color: #009cde;  font-weight: 200; margin: 0 0 10px 0; ">
                                                         {{ $rec_rel['nombre'] }}
                                                    </h5>
                                                    <h6 style="  font-weight: 200; ">
                                                         {{ $rec_rel['descripcion'] }}
                                                    </h6>

                                               </div>
                                          </div>

                                     </div>
                              @endforeach






                            </div>



                        </div>

                         <?php } ?>

                    </div>

                </div>

                <center>
                    <?php if (is_null($ultimo_intento['fin'])): ?>
                         <?php if ($ultim_curso == 'fin'): ?>
                              <a href="{{url('/examen/'.$examen['id'])}}" class="btn btn-primary btn-lg active" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;">Realizar evaluación <i class=" fa fa-arrow-circle-right" style="color:#fff;"></i> </a>
                         <?php endif; ?>
                    <?php else: ?>
                              <?php if ($ultimo_intento['aprobado'] != 1): ?>
                                   <a href="{{url('/reiniciar_curso/'.$curso['id'])}}" class="btn btn-primary btn-lg active" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;">Reiniciar curso <i class=" fa fa-arrow-circle-right" style="color:#fff;"></i> </a>
                              <?php endif; ?>

                    <?php endif; ?>

                    <?php if ($ultim_curso != 'fin'): ?>
                         <a href="{{url('/ver_material/'.$ultim_curso[0])}}" class="btn btn-primary btn-lg active" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;">Continuar con el curso <i class=" fa fa-arrow-circle-right" style="color:#fff;"></i> </a>
                    <?php endif; ?>
                </center>

            </div>
        </div>





    </div>






</section>













@endsection
