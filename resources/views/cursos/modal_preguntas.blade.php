<div id="modal-form" class="modal fade" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <form method="post"  id="form-ajax" action="{{ url('/fin_material/'.$material['id']) }}" style="text-align: center;">
                        @if(isset($preguntas))
                        @foreach ($preguntas as $key1 => $pregunta)

                        <div class="col-md-12">
                            <div class="col-md-12 m-b-md">
                                <h5 style="margin:0px;">{{ $pregunta['pregunta']['titulo'] }}</h5>
                            </div>

                            
                            
                            {!! $pregunta['pregunta']['descripcion'] !!}
                            
                            
                            <?php
                            $arr = array('a', 'b', 'c', 'd', 'e', 'f');
                            $i = 0;
                            ?>


                            @foreach ($pregunta['respuesta'] as $key2 => $respuestas)
                            <div class="col-md-12" style="margin-top:15px; text-align: left;">
                                
                                    
                                
                                    <input type="radio" name="{{ $pregunta['pregunta']['id'] }}" value=" {{ $respuestas['id'] }}" required > 
                                    
                                    
                                    <?php echo $arr[$i].')'; ?>
                                  
                                    {{ $respuestas['opcion'] }} 
                                 
                                
                                 <?php ++$i; ?>
                                
                               
                                    </div>
                                    @endforeach
                                    </div>
                                    <br>
                                    <div class="col-md-12">
                                        <hr>
                                    </div>
                                    <br>
                                   
                                    @endforeach
                                    @endif
                                    <div class="col-ms-12">
                                        <div class="col-ms-12">
                                            <div class="col-md-6">
                                                <button type="button" style="width:100%;"  class="button button-small button-circle button-red cerrar_modal_preguntas " >Regresar</button>
                                            </div>
                                            <div class="col-md-6">
                                                <button type="submit"  style="width:100%;"  class="button button-small button-circle button-green" >Enviar</button>
                                            </div>
                                        </div>

                                    </div>
                                    </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
