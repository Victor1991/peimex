@extends('layouts.app')

@section('title', 'Main page')

@section('content')


<section id="content" style="background-color: #faf9fe; ">

     <div class="container clearfix">

          <br>
          <div class="row">
               <div class="col-md-6">
                    <a href="{{ url('/curso/'.$curso['info']['id']) }}" class="button button-mini button-dark button-rounded"><i class="icon-reload"></i>Regresar</a>
               </div>
               <div class="col-md-6" style="text-align: right; margin-top: 20px;">
                    <ol class="breadcrumb">
                         <li class="breadcrumb-item"><a href="{{ url('/home') }}">Cursos</a></li>
                         <li class="breadcrumb-item"><a href="{{ url('/curso/'.$curso['info']['id']) }}">{{ $curso['info']['nombre'] }}</a></li>
                         <li class="breadcrumb-item active" aria-current="page">{{ $material['titulo'] }}</li>
                    </ol>
               </div>
          </div>

          <hr>


          <div class="row" style="margin-top: 20px;">

               <div class="col-md-4" >


                    <div class="jumbotron " style=" background-color: #242f44; padding:20px; line-height: .7; background-repeat:no-repeat; border-radius: 10px;" >
                         <div class="row">
                              <div class="col-md-12">


                                   <span style="font-weight: 200; font-size: 22px; color: #fff">{{ $curso['porcentaje']['1'] }} Módulos</span>
                                   <hr>
                                   <small class="pull-left" style="color: #fff; font-size: 14px;">Completado: {{ $curso['porcentaje'][2]}} módulos de  {{ $curso['porcentaje'][1]}}</small>
                                   <br>
                                   <div class="progress progress-small" style="height: 10px;">
                                        <div style="width: {{ $curso['porcentaje'][0] }}%;" class="progress-bar"></div>
                                   </div>
                              </div>

                         </div>
                    </div>



                    @include('cursos.secciones')
               </div>
               <div class="col-md-8">

                    <div class="jumbotron shadow "
                         style="
                         <?php if ($curso['info']['imagen_small']): ?>
                              background-image: url('https://capapei.com/material/principal/{{$curso['info']['imagen_small']}}');
                         <?php else: ?>
                              background-image: url('https://capapei.com/material/principal/b_sin_imagen.png');
                         <?php endif; ?>
                         padding-top:40px;
                         padding-bottom:40px;
                         background-repeat:no-repeat;
                         background-position:center;
                         background-size:cover;
                         -webkit-background-size:cover;
                         border-radius: 10px;" >
                         <div class="row">
                              <div class="col-md-12">

                                  <span class="tit1" style="color:#">{{ $curso['info']['nombre'] }}</span><br>

                                   <!--<p style="font-size: 16px; text-align: justify; color: #fff; line-height: 1.4;">{!! $curso['info']['descripcion'] !!}</p>-->
                              </div>

                         </div>
                    </div>

                    <div class="clearfix"></div>


                    <div class="col-md-12  shadow" style="background-color: #fff; margin-top: 20px; border-radius: 10px; padding:15px;">
                         <div class=" col-md-12 " style="font-size: 18px;">
                              <?php if ($material['objetivos']):?>
                                   <strong>Objetivos de Aprendizaje:</strong>
                                   <div class=" col-md-12 ">
                                        <div class="col-sm-12">{!! $material['objetivos'] !!}</div>
                                   </div>
                                   <br>
                              <?php endif; ?>
                              <?php if ($material['termino']):?>
                                   <strong>Al terminar la actividad, el participante:</strong>
                                   <div class=" col-md-12 ">
                                        <div class="col-sm-12">{!! $material['termino'] !!}</div>
                                   </div>
                                   <br>
                              <?php endif; ?>

                              <?php if ($material['infografia'] ||  $material['referencias']):?>
                                   <strong>Archivos descargables y materiales de consulta:</strong>
                                   <br>
                                   <div class=" col-md-12 ">
                                        <?php if ($material['infografia']): ?>
                                             <!-- <a href="#"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Infografía </a> -->
                                        <?php endif; ?>
                                        <?php if ($material['referencias']): ?>
                                             <a href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Referencias </a>
                                        <?php endif; ?>

                                   </div>
                                   <br>
                              <?php endif; ?>

                              <!-- <?php if($material['infografia'] != ''){ ?>

                                   <div class="col-sm-12"><a href="{!! asset('material/pdf/'.$material['infografia']) !!}" target="_blank"><i class="fa fa-file-pdf-o"></i>&nbsp;&nbsp;Descargar </a> </div>

                              <?php } ?> -->


                         </div>
                    </div>


                    <div class="clearfix"></div>
                    <br>

                    <div class="col-md-12 shadow" style="background-color: #fff; border-radius: 10px; padding: 2px;">
                         <div class="col-md-12 " style="margin-bottom: 20px; margin-top: 20px;  ">
                              @if ($material['tipo_archivo'] == "specialkey" )
                              <form id="regForm" class="specialform" method="post" action="{{ route('save_especialkey', ['id' => $material['id']]) }}">

                                   @foreach ($specialkey as $key1 => $pregunta)

                                   <div class="tab">
                                        <div class="row">

                                             <div class="col-md-12">
                                                  <?php echo $pregunta['pregunta']['descripcion']; ?>
                                             </div>
                                             <div class="col-md-12 m-b-md text-center">

                                                 <h5 style="color: #598df5; font-weight: 400; font-size: 20px; ">{{ $pregunta['pregunta']['titulo'] }}</h5>
                                             </div>
                                             @foreach ($pregunta['respuesta'] as $key2 => $respuestas)
                                             <div class="col-md-12  ">
                                                  <label class="container" id="{{ $respuestas['id']  }}"  style="font-size:14px;">
                                                       <p style="margin-left:18px;"><?= $respuestas['opcion'] ?></p>
                                                       <input type="radio"  correcta="{{ $respuestas['correcta'] }}"  preg_id="{{ $respuestas['id']  }}"  class="resp_especialkey resp{{ $pregunta['pregunta']['id'] }}" mensaje="{{ $respuestas['descripcion'] }}" pregunta="{{ $pregunta['pregunta']['id'] }}" name="{{ $pregunta['pregunta']['id'] }}" value="{{ $respuestas['id'] }}" required >
                                                       <span class="checkmark"></span>
                                                  </label>
                                             </div>
                                             @endforeach
                                        </div>
                                        <hr>
                                        <div id="respuesta{{ $pregunta['pregunta']['id'] }}">

                                        </div>
                                   </div>
                                   @endforeach

                                   <br>
                                   <div style="overflow:auto;">

                                        @php
                                        $class = 'btn-modal';
                                        /* materiales terminados */
                                        $terminados =  array_column($curso['porcentaje'][3], 'material_id');

                                        if(!isset($preguntas)){
                                             $class = '';
                                        }

                                        if (in_array( $material['id'], $terminados)) {
                                             $class = '';
                                        }


                                        if (in_array( $material['id'], $terminados)) {
                                             $class = '';
                                        }


                                        if ($material['tipo_archivo'] == "specialkey") {
                                             $class = 'enviar-special';
                                        }

                                        @endphp

                                        <div class="">

                                             <?php if ($siguiente): ?>
                                                  <?php if(isset($preguntas)):  ?>
                                                  <a href="{{ url('/ver_material/'.$siguiente) }}" class="btn btn-primary {{ $class }} pull-right " style="border-radius: 20px; margin-left: 10px; background-color: #2d7fd2; border-color: #2d7fd2;"> Siguiente módulo </a>
                                                  <?php else: ?>
                                                  <a href="{{ url('/fin_material_sin_preguntas/'.$siguiente) }}" class="btn btn-primary {{ $class }} pull-right " style="border-radius: 20px; margin-left: 10px; background-color: #2d7fd2; border-color: #2d7fd2;"> Siguiente módulo </a>
                                                  <?php endif; ?>
                                             <?php else: ?>
                                                  <a href="{{  url('/') }}" class="btn btn-primary {{ $class }} pull-right m-l-sm m-r-sm" style="border-radius: 20px; background-color: #2d7fd2; margin-left: 10px; border-color: #2d7fd2;"> Terminar <i class="fa fa-trophy"></i>  </a>
                                             <?php endif; ?>
                                        </div>

                                        <div style="float:right;">
                                             <button type="button " id="prevBtn"  onclick="nextPrev(-1)">Pregunta anterior</button>
                                             <button type="button" id="nextBtn" onclick="nextPrev(1)">Siguiente</button>
                                        </div>


                                        <div class="">
                                             <?php if ($anterior): ?>
                                                  <a type="button" href="{{ url('/ver_material/'.$anterior) }}" class="btn btn-primary  pull-right"  style="border-radius: 20px; background-color: #2d7fd2; margin-right: 10px; border-color: #2d7fd2;">Módulo anterior</a>
                                             <?php else: ?>

                                             <?php endif; ?>

                                        </div>
                                   </div>
                                   <button type="submit" style="display:none;" id="submit_special"></button>
                              </form>


                              <button type="submit" id="submit_special"  style="display:none;">info</button>

                         </form>


                         @elseif($material['tipo_archivo'] == "video")
                         <video width="100%"  controls controlsList="nodownload">
                                   <source src=" https://capapei.com/material/video/{!! $material['archivo'] !!}" type="video/mp4">
                              </video>


                              @elseif($material['tipo_archivo'] == "pdf")
                              <!-- <embed src="{!! asset('material/pdf/'.$material['archivo']) !!}"  style="width:100%; height:600px; "></embed > -->


                              <div class="embed-responsive embed-responsive-16by9">
				                <iframe class="embed-responsive-item" src="https://capapei.com/material/video/{!! $material['archivo'] !!}" allowfullscreen></iframe>
          			      </div>


                              @endif

                              <br>


                              @php
                              $class = 'btn-modal';
                              /* materiales terminados */
                              $terminados =  array_column($curso['porcentaje'][3], 'material_id');

                              if(!isset($preguntas)){
                                   $class = '';
                              }

                              if (in_array( $material['id'], $terminados)) {
                                   $class = '';
                              }


                              if (in_array( $material['id'], $terminados)) {
                                   $class = '';
                              }


                              if ($material['tipo_archivo'] == "specialkey") {
                                   $class = 'enviar-special';
                              }


                              @endphp
                              <?php if($material['tipo_archivo'] != "specialkey" ): ?>
                                   <?php if ($siguiente): ?>
                                        <?php if(isset($preguntas)):  ?>
                                        <a href="{{ url('/ver_material/'.$siguiente) }}" class="btn btn-primary {{ $class }} pull-right " style="border-radius: 20px; margin-left: 10px; background-color: #2d7fd2; border-color: #2d7fd2;"> Siguiente módulo </a>
                                        <?php else: ?>
                                        <a href="{{ url('/fin_material_sin_preguntas/'.$material['id']) }}" class="btn btn-primary {{ $class }} pull-right " style="border-radius: 20px; margin-left: 10px; background-color: #2d7fd2; border-color: #2d7fd2;"> Siguiente módulo </a>
                                        <?php endif; ?>
                                   <?php else: ?>
                                        <a href="{{  url('/fin_material_sin_preguntas/'.$material['id']) }}" class="btn btn-primary {{ $class }} pull-right m-l-sm m-r-sm" style="border-radius: 20px; background-color: #2d7fd2; border-color: #2d7fd2;"> Terminar <i class="fa fa-trophy"></i>  </a>
                                   <?php endif; ?>

                                   <?php if ($anterior): ?>
                                        <a type="button" href="{{ url('/ver_material/'.$anterior) }}" class="btn btn-primary  pull-right m-l-sm m-r-sm"  style="border-radius: 20px; background-color: #2d7fd2; margin-right: 10px; border-color: #2d7fd2;"> Anterior</a>
                                   <?php else: ?>

                                   <?php endif; ?>
                              <?php endif; ?>

                         <div class="clearfix"></div>
                         </div>
                    </div>

                    <div class="clearfix"></div>



               </div>

          </div>
     </div>

     <!-- Modal -->
     <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

               <!-- Modal content-->
               <div class="modal-content" style="border-radius: 20px 20px 10px 10px;">
                    <div class="modal-header" style="background-color: #3382d2; border-radius: 10px 10px 0px 0px;">
                         <button type="button" class="close bt_cerrar" data-dismiss="modal">&times;</button>
                         <h4 class="modal-title" style="color:#fff;">Referencias</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-12" style="text-align: justify;">
                              {!! $material['referencias'] !!}
                         </div>
                         <div class="clearfix"></div>
                    </div>
               </div>

          </div>
     </div>

</section>




@include('cursos.modal_preguntas')

@endsection
