<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie10 lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie10 lt-ie9"> <![endif]-->
<!--[if IE 9]><html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
<!--<![endif]-->

<head>

     <!-- Basic Page Needs -->
     <meta charset="utf-8">
     <title>CapaPEI</title>
     <meta name="description" content="Desarrollando talento">
     <meta name="author" content="Peimex">

     <!-- Mobile Specific Metas -->
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" <!-- Favicons -->
     <link rel="shortcut icon" href="img/ico.ico">

     <!-- FONTS -->
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,400italic,700'>
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Patua+One:100,300,400,400italic,700'>
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,900'>
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Poppins:100,300,400,400italic,700'>
     <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Nunito:100,300,400,400italic,700'>

     <!-- CSS -->
     <link rel='stylesheet' href="{!! asset('public/pagina/css/global.css') !!}">
     <link rel='stylesheet' href="{!! asset('public/pagina/css/structure2.css') !!}">
     <link rel='stylesheet' href="{!! asset('public/pagina/css/sitter2.css') !!}">
     <link rel='stylesheet' href="{!! asset('public/pagina/css/custom2.css') !!}">

     <!-- Revolution Slider -->
     <link rel="stylesheet" href="plugins/rs-plugin-5.3.1/css/settings.css">

</head>

<body class="home page-template-default page template-slider  color-custom style-simple button-default layout-full-width one-page if-overlay no-shadows header-transparent sticky-header sticky-tb-color ab-hide subheader-both-center menu-link-color menuo-right menuo-no-borders mobile-tb-center mobile-mini-mr-ll tablet-sticky mobile-header-mini mobile-sticky">
     <div id="Wrapper">
          <div id="Header_wrapper">
               <header id="Header">
                    <div id="Top_bar" style="background-color: #fff;">
                         <div class="container">
                              <div class="column one">
                                   <div class="top_bar_left clearfix">
                                        <div class="logo">
                                             <a id="logo" href="#" title="eduPlace" data-height="60" data-padding="15">
                                                  <img class="logo-main scale-with-grid" src="{!! asset('public/pagina/img/logo.png') !!}" data-retina="{!! asset('public/pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina>
                                                  <img class="logo-sticky scale-with-grid" src="{!! asset('public/pagina/img/logo.png') !!}" data-retina="{!! asset('public/pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina>
                                                  <img class="logo-mobile scale-with-grid" src="{!! asset('pag     ina/img/logo.png') !!}" data-retina="{!! asset('public/pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina>
                                                  <img class="logo-mobile-sticky scale-with-grid" src="{!! asset('public/pagina/img/logo.png') !!}" data-retina="{!! asset('public/pagina/img/logo.png') !!}" data-height="37" alt="eduPlace" data-no-retina></a>
                                        </div>
                                        <div class="menu_wrapper">
                                             <nav id="menu">
                                                  <ul id="menu-menu" class="menu menu-main">
                                                       <li>
                                                            <a href="{{ url('/login') }}"><span>INICIAR SESIÓN</span></a>
                                                       </li>
                                                  </ul>
                                             </nav><a class="responsive-menu-toggle" href="#"><i class="icon-menu-fine"></i></a>
                                        </div>
                                   </div>

                              </div>
                         </div>
                    </div>
                    <div class="mfn-main-slider" id="mfn-rev-slider">
                         <div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-source="gallery" style="margin:0px auto;background:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                              <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.8.3">
                                   <ul>
                                        <li data-index="rs-3" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0" data-saveperformance="off" data-title="Slide"
                                        data-param1 data-param2 data-param3 data-param4 data-param5 data-param6 data-param7 data-param8 data-param9 data-param10 data-description>
                                        <img src="{!! asset('public/pagina/img/slider1.png') !!}" title="sitter2-home-slider-bg2" width="1920" height="937" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>








                                        <div class="tp-caption" id="slide-3-layer-27" data-x="center" data-y="170" data-width="['auto']" data-height="['auto']" data-type="image" data-responsive_offset="on" data-frames='[{"delay":0,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                        data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 9; ">
                                        <img src="{!! asset('public/pagina/img/logo2.png') !!}">

                                   </div>



                                   <div class="tp-caption" id="slide-3-layer-5" data-x="center" data-y="350" data-width="['auto']" data-height="['auto']" data-type="text" data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                                   data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7; white-space: nowrap; font-size: 28px; line-height: 28px; font-weight: 200; color: #fff; letter-spacing: 0px;">
                                   <center>

                                        CapaPEI,
                                         desarrollando talento
                                   </center>
                              </div>
                              <a href="{{ url('/login') }}" class="tp-caption rev-btn  tp-resizeme" id="slide-3-layer-6" data-x="center" data-y="430" data-width="['auto']" data-height="['auto']" data-type="button" data-actions='[{"event":"click","action":"scrollbelow","offset":"-100px","delay":"200","speed":"600","ease":"Linear.easeNone"}]'
                              data-responsive_offset="on" data-frames='[{"delay":10,"speed":300,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(255,255,255);bg:rgb(40,168,65);bs:solid;bw:0 0 0 0;"}]'
                              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[12,12,12,12]" data-paddingright="[35,35,35,35]" data-paddingbottom="[12,12,12,12]" data-paddingleft="[35,35,35,35]" style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 20px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: px;font-family:Nunito;background-color:rgb(0,156,222);border-color:rgba(0,0,0,1);border-radius:30px 30px 30px 30px; margin-top:16px; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">
                              Iniciar sesión
                         </a>

               </li>

          </ul>
          <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
     </div>
</div>
</div>
</header>
</div>
<div id="Content">
     <div class="content_wrapper clearfix">
          <div class="sections_group">
               <div class="entry-content">






                    <div class="section mcb-section mcb-section-9w4om4i9s" id="work" style="padding-top:40px;padding-bottom:25px;background-color:#faf9fe;">
                         <div class="section_wrapper mcb-section-inner">

                              <div class="wrap mcb-wrap mcb-wrap-816gse6yc one-third  valign-top clearfix" style="padding:0 2%">
                                   <div class="mcb-wrap-inner">
                                        <div class="column mcb-column mcb-item-1icjlcvbz one column_icon_box">
                                             <div class="icon_box icon_position_top no_border">

                                                  <center> <img src="{!! asset('public/pagina/img/icon4.png') !!}"></center>


                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div class="wrap mcb-wrap mcb-wrap-87gbte1zy one-third  valign-top clearfix" style="padding:0 2%">
                                   <div class="mcb-wrap-inner">
                                        <div class="column mcb-column mcb-item-n1x859fk1 one column_icon_box">
                                             <div class="icon_box icon_position_top no_border">
                                                  <center><img src="{!! asset('public/pagina/img/icon5.png') !!}"></center>

                                             </div>
                                        </div>
                                   </div>
                              </div>
                              <div class="wrap mcb-wrap mcb-wrap-1ld2bqq3r one-third  valign-top clearfix" style="padding:0 2%">
                                   <div class="mcb-wrap-inner">
                                        <div class="column mcb-column mcb-item-ixxejw4f3 one column_icon_box">
                                             <div class="icon_box icon_position_top no_border ">

                                                  <center>
                                                       <img src="{!! asset('public/pagina/img/icon6.png') !!}">
                                                  </center>



                                             </div>
                                        </div>
                                   </div>
                              </div>

                             <div class="wrap mcb-wrap mcb-wrap-8545a4r3b one  valign-top clearfix">
                                   <div class="mcb-wrap-inner">

                                        <div class="column mcb-column mcb-item-aahg8szpi one column_column column-margin-10px">
                                             <div class="column_attr clearfix align_center" >

                                                  <h4 style="color:#454545;">- Descubre todas las ventajas que CapaPEI tiene para ti - </h4>
                                             </div>
                                        </div>

                                   </div>
                              </div>
                         </div>
                    </div>

               </div>
          </div>
     </div>
</div>
<footer id="Footer" class="clearfix" style="padding-top:20px;">
     <div class="footer_copy">
          <div class="container">

               <div class="column three-fifth">




                    <img src="{!! asset('public/pagina/img/logo3.png') !!}" style="max-width: 75px;" ><br>
                    <span style="font-size: 11px; color: #454545; font-weight: 300;">&copy; 2019 PEIMEX. Todos los derechos reservados.</span>


               </div>
               <div class="column one-fifth">

                   <!--
                    <h4 style="font-size: 20px;line-height: 29px; font-weight: 200; letter-spacing: 0px; color: #4d6184;">CONECTAR</h4>
                    Blog<br>
                    Facebook<br>
                    LinkedIn<br>
                    Twitter<br>
                    Youtube<br>
                    -->

               </div>
               <div class="column one-fifth">

                   <!-- <a href="" style="color:#454545">Privacidad</a><br>
                     <a href="" style="color:#454545">Preguntas Frecuentes</a><br>
                     <a href="" style="color:#454545">Contacto</a><br> -->
                    <a id="back_to_top" class="button button_js" href><i class="icon-up-open-big"></i></a>
               </div>


          </div>
     </div>
</footer>
</div>


<!-- JS -->
<script src="{!! asset('public/pagina/js/jquery-2.1.4.min.js') !!}"></script>
<script src="http://code.jquery.com/jquery-migrate-1.2.1.min.js"></script>


<script src="{!! asset('public/pagina/js/mfn.menu.js') !!}"></script>
<script src="{!! asset('public/pagina/js/jquery.plugins.js') !!}"></script>
<script src="{!! asset('public/pagina/js/jquery.jplayer.min.js') !!}"></script>
<script src="{!! asset('public/pagina/js/animations/animations.js') !!}"></script>
<script src="{!! asset('public/pagina/js/translate3d.js') !!}"></script>
<script src="{!! asset('public/pagina/js/scripts.js') !!}"></script>
<script src="{!! asset('public/pagina/js/email.js') !!}"></script>

<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/jquery.themepunch.tools.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/jquery.themepunch.revolution.min.js') !!}"></script>

<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.video.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.slideanims.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.actions.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.layeranimation.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.kenburn.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.navigation.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.migration.min.js') !!}"></script>
<script src="{!! asset('public/pagina/plugins/rs-plugin-5.3.1/js/extensions/revolution.extension.parallax.min.js') !!}"></script>

<script>
var revapi1, tpj;
( function() {
     if (!/loaded|interactive|complete/.test(document.readyState))
     document.addEventListener("DOMContentLoaded", onLoad);
     else
     onLoad();
     function onLoad() {
          if (tpj === undefined) {
               tpj = jQuery;
               if ("off" =="on")
               tpj.noConflict();
          }
          if (tpj("#rev_slider_1_1").revolution == undefined) {
               revslider_showDoubleJqueryError("#rev_slider_1_1");
          } else {
               revapi1 = tpj("#rev_slider_1_1").show().revolution({
                    sliderType :"standard",
                    sliderLayout :"auto",
                    dottedOverlay :"none",
                    delay : 3000,
                    navigation : {
                         onHoverStop :"off",
                    },
                    visibilityLevels : [1240, 1024, 778, 480],
                    gridwidth : 1080,
                    gridheight : 768,
                    lazyType :"none",
                    shadow : 0,
                    spinner :"spinner2",
                    stopLoop :"off",
                    stopAfterLoops : -1,
                    stopAtSlide : -1,
                    shuffle :"off",
                    autoHeight :"off",
                    disableProgressBar :"on",
                    hideThumbsOnMobile :"off",
                    hideSliderAtLimit : 0,
                    hideCaptionAtLimit : 0,
                    hideAllCaptionAtLilmit : 0,
                    debugMode : false,
                    fallbacks : {
                         simplifyAll :"off",
                         nextSlideOnWindowFocus :"off",
                         disableFocusListener : false,
                    }
               });
          };
     };
}());
</script>


</body>

</html>
