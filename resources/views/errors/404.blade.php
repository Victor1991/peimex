<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>PHARMA | 404 Error</title>

    <link rel="stylesheet" href="{!! asset('public/css/vendor.css') !!}" />
    <link rel="stylesheet" href="{!! asset('public/css/app.css') !!}" />

</head>

<body class="gray-bg">


    <div class="middle-box text-center animated fadeInDown">
        <h1>404</h1>
        <img src="{!! asset('public/img/be_logo.png') !!}" alt="">

        <h3 class="font-bold">Página no encontrada</h3>
        <div class="error-desc">
           Lo sentimos, pero la página que estás buscando no ha sido encontrada. Intente verificar la URL para ver si hay errores
        </div>
          <br>
        <div style="text-align: center;">
          <h3><a href="{{ url('/home') }}"> Ir al incio </a></h3>
        </div>
    </div>

    <!-- Mainly scripts -->
    <script src="public/js/jquery-2.1.1.js"></script>
    <script src="public/js/bootstrap.min.js"></script>

</body>

</html>
